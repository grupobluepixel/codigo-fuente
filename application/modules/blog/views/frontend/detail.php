<?php 
	$page = $this->load->view($_SESSION['lang'].'/blog-detalle',array(),TRUE,'paginas');
	foreach($detail as $n=>$d){
        $page = str_replace('['.$n.']',$d,$page);
    }
    $page = str_replace('[comentarios]',$comentarios->num_rows(),$page);
    $page = str_replace('[vistas]',0,$page);
    $page = str_replace('[aside]',$this->load->view('frontend/aside',array('recientes'=>$relacionados,'tags'=>explode(',',$detail->tags)),TRUE),$page);
    $page = str_replace('[prev]',$prev,$page);
    $page = str_replace('[next]',$next,$page);
    $page = $this->querys->fillFields($page,array('relacionados'=>$relacionados));
    $page = $this->load->view('read',array('page'=>$page,'link'=>'blog'),TRUE,'paginas');
    
    echo $page;
?>