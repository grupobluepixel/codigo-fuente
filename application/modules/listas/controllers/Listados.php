<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Listados extends Main{
        function __construct() {
            parent::__construct();
        }

        function categorias(){
			$this->load->library('grocery_crud');
			$this->load->library('ajax_grocery_crud');
			$crud = new ajax_grocery_crud();
			$crud->set_table('categorias')
				 ->set_theme('categorias_listado')				 
				 ->columns('id','nombre','subcategorias','total')	
				 ->callback_column('subcategorias',function($val,$row){
				 	$this->db->order_by('nombre','ASC');
				 	return $this->db->get_where('sub_categorias',array('categorias_id'=>$row->id));
				 })
				 ->callback_column('total',function($val,$row){
				 	return $this->db->get_where('listas_categorias',array('categorias_id'=>$row->id))->num_rows();
				 })
				 ->unset_add()
				 ->unset_edit()
				 ->unset_delete()
				 ->unset_read()
				 ->unset_print()
				 ->unset_export()
				 ->unset_jquery()
				 ->order_by('nombre','ASC');
			$crud->set_url('listas/listados/categorias/');
			$crud = $crud->render('','theme/theme/views/listados/'); 
			$this->loadView(array('view'=>'views/listas','title'=>'Categorias','js_files'=>$crud->js_files,'css_files'=>$crud->css_files,'output'=>$crud->output));
    	}

        function subcategorias($id){
        	$param = $id;
        	$id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
            	$categoria = $this->db->get_where('categorias',array('id'=>$id));
            	if($categoria->num_rows()>0){
            		$categoria = $categoria->row();                  		
		            $this->loadView(array('view'=>'views/categorias-interna','categoria'=>$categoria,'title'=>'Categoria '.$categoria->nombre));
	        	}
            }       
    	}

    	function listas($id = ''){
    		$this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
        	$param = $id;
        	$id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
            	$this->id = $id;
            	$this->db->select('sub_categorias.*, categorias.id as catid, categorias.nombre as catnom');
            	$this->db->join('categorias','categorias.id = sub_categorias.categorias_id');
            	$categoria = $this->db->get_where('sub_categorias',array('sub_categorias.id'=>$id));
            	if($categoria->num_rows()>0){
            		$categoria = $categoria->row();
            		$this->categoria = $categoria;	            	
		            $crud = new ajax_grocery_crud();
		            $crud->set_table('listas_categorias')		            	 
		                 ->set_theme('subcategorias')
		                 ->group_by('listas_id')
		                 ->columns('lista','categoria','total')
		                 ->callback_column('lista',function($val,$row){return $this->querys->get_listas(array('listas.id'=>$row->listas_id));})
		                 ->callback_column('categoria',function($val,$row){return $this->categoria;})
		                 ->callback_column('total',function($val,$row){$this->db->group_by('listas_id'); return $this->db->get_where('listas_categorias',array('subcategorias_id'=>$this->id))->num_rows();})
		                 ->where('subcategorias_id',$categoria->id)
		                 ->unset_add()
		                 ->unset_edit()
		                 ->unset_delete()
		                 ->unset_read()
		                 ->unset_print()
		                 ->unset_export()
		                 ->unset_jquery() 
		                 ->order_by('nombre','ASC');
		            $crud->set_url('listas/listados/listas/'.$param.'/');
		            $crud = $crud->render('','theme/theme/views/listados/'); 
		            $this->loadView(array('view'=>'views/listas','categoria'=>$categoria,'title'=>'Subcategoria '.$categoria->nombre,'js_files'=>$crud->js_files,'css_files'=>$crud->css_files,'output'=>$crud->output));		
	        	}
            }else{
            	$crud = new ajax_grocery_crud();
	            $crud->set_table('listas')	                 
	            	 ->set_theme('listas')
	            	 ->set_subject('listas')
	            	 ->columns('id','titulo','url','titulo_corto')
	            	 ->display_as('id','id')
	            	 ->display_as('titulo','titulo')
	            	 ->display_as('url','url')
	            	 ->display_as('titulo_corto','titulo_corto')
	            	 ->callback_column('url',function($val,$row){
	            	 	return base_url('lista/'.toUrl($row->id.'-'.$row->titulo_corto));
	            	 })
	                 ->unset_add()
	                 ->unset_edit()
	                 ->unset_delete()
	                 ->unset_read()
	                 ->unset_print()
	                 ->unset_export()
	                 ->unset_jquery();	            
	            $crud = $crud->render('','theme/theme/views/listados/'); 
	            $this->loadView(array('view'=>'views/listas','title'=>'Listas','js_files'=>$crud->js_files,'css_files'=>$crud->css_files,'output'=>$crud->output));		
	        	
            }
    	}

    	function buscador(){
    		$this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
    		$crud = new ajax_grocery_crud();
            $crud->set_table('view_buscador')	                 
            	 ->set_theme('listas')
            	 ->set_subject('listas')
            	 ->columns('id','titulo','url','titulo_corto')
            	 ->display_as('id','id')
            	 ->display_as('titulo','titulo')
            	 ->display_as('url','url')
            	 ->display_as('titulo_corto','titulo_corto')
            	 ->callback_column('url',function($val,$row){
            	 	return base_url('lista/'.toUrl($row->id.'-'.$row->titulo_corto));
            	 })
                 ->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->unset_export()
                 ->unset_jquery()
                 ->group_by("id");	     

            if (!empty($_POST["busqueda"])) {
            	$crud->or_like('titulo',$_POST["busqueda"])
            		 ->or_like('titulo_corto', $_POST["busqueda"])
            		 ->or_like('nombre',$_POST["busqueda"]);
            }

            $crud = $crud->render('','theme/theme/views/listados/'); 
            $this->loadView(array('view'=>'views/listas','title'=>'Listas','js_files'=>$crud->js_files,'css_files'=>$crud->css_files,'output'=>$crud->output));		
    	}

    	function denuncias($id){
    		$this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
			$crud = new ajax_grocery_crud();
		    $crud->set_table('denuncias')
		         ->set_theme('bootstrap2')
		         ->required_fields_array()		         
		         ->unset_edit()
		         ->unset_delete()
		         ->unset_read()
		         ->unset_print()
		         ->unset_export()
		         ->unset_jquery()
		         ->unset_list()
		         ->callback_before_insert(function($post){
		         	$post['fecha'] = date("Y-m-d H:i:s");
		         	return $post;
		         });		    
		    $crud = $crud->render(); 
    }
}