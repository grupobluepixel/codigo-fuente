<div>[menu]</div>
<!-- Header -->
<!-- Header -->


    <!-- Content -->
    <div id="content">

        <!-- Page Title -->
        <div class="page-title page-title-lg bg-dark dark">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 push-lg-4">
                        <h1 class="mb-0">NOTÍCIES</h1>
                        <h4 class="text-muted mb-0">Informacions dels nostres tallers i teràpies</h4>
                    </div>
                </div>
            </div>
        </div>

        <!-- Page Content -->
        <div class="page-content bg-light">

            <div class="container clearfix">
                <div class="main left">
                    
                    <!-- Post / Item -->
                    [foreach:blog]
                        <article class="post post-wide animated" data-animation="fadeIn">
                            <div class="post-image"><img src="[foto]" alt=""></div>
                            <div class="post-content">
                                <ul class="post-meta">
                                    <li>[fecha]</li>
                                    <li>per [user]</li>
                                </ul>
                                <h4><a href="[link]">[titulo]</a></h4>
                                <p>[texto]</p>
                            </div>
                        </article>
                    [/foreach]
                    <!-- Pagination -->
                    [paginacion]
                </div>
                <div class="sidebar right">
                    <!-- Widget - Newsletter -->
                    <div class="widget widget-newsletter">
                        <h5>Vols rebre les nostres noticies?</h5>
                        <form action="//suelo.us12.list-manage.com/subscribe/post-json?u=ed47dbfe167d906f2bc46a01b&amp;id=24ac8a22ad" id="sign-up-form2" class="sign-up-form validate-form" method="POST"
                            data-message-error="Opps ... S'ha produït un error. Torna-ho a provar més tard"
                            data-message-success="Sí! Aviat rebràs un correu electrònic de confirmació ..."
                        >
                            <div class="form-group mb-0">
                                <input type="email"  name="EMAIL" id="mce-EMAIL2" value="" class="form-control" placeholder="El teu email..." required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-secondary btn-block">
                                    <span>Suscriu-te!</span>
                                </button>
                            </div>
                        </form>
                    </div>
                    <hr>
                    <!-- Widget - Recent posts -->
                    <div class="widget widget-recent-posts">
                        <h5>Ultimes noticies</h5>
                        <ul class="list-posts">
                            [foreach:recientes]
                                <li>
                                    <a href="[link]" class="title">[titulo]</a>
                                    <span class="date">[fecha]</span>
                                </li>
                            [/foreach]
                        </ul>
                    </div>
                    <hr>
                    <!-- Widget - Twitter -->
                    <!--<div class="widget widget-twitter">
                        <h5>Latest Tweets</h5>
                        <div id="twitter-feed" class="twitter-feed"></div>
                    </div>-->
                </div>
            </div>
            
        </div>

        <div>[footer]</div>

    </div>
    <!-- Content / End -->