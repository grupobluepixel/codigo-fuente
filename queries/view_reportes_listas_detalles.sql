DROP VIEW IF EXISTS view_reportes_listas_detalles;
CREATE VIEW view_reportes_listas_detalles AS
SELECT
ldo.id,
ldo.listas_id,
ldo.adjunto,
ldo.tipo_adjunto,
ldo.nombre,
ldo.autor,
ldo.votos_originales
FROM listas_detalles AS ldo