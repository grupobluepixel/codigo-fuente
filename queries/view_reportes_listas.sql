DROP VIEW IF EXISTS view_reportes_listas;
CREATE VIEW view_reportes_listas AS
SELECT
listas.id as id,
TIME(fecha) AS hora,
DATE(fecha) AS fecha,
user.nickname AS usuario,
listas.titulo,
CAT1.categoria AS cat1,
CAT1.subcategoria AS subcat1,
CAT2.categoria AS cat2,
CAT2.subcategoria AS subcat2
FROM
listas
INNER JOIN user ON user.id = listas.user_id
LEFT JOIN view_get_categoria AS CAT1 ON CAT1.listas_id = listas.id
LEFT JOIN view_get_categoria2 AS CAT2 ON CAT2.listas_id = listas.id AND CAT2.id!=CAT1.id
GROUP BY listas.id