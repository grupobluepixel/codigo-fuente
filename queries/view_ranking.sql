DROP VIEW IF EXISTS view_ranking;
CREATE VIEW view_ranking AS 
SELECT
listas.*,
categorias.id as categorias_id,
categorias.nombre as catnom,
sub_categorias.id as subcategorias_id,
sub_categorias.nombre as subcatnom
FROM listas
RIGHT JOIN listas_categorias ON listas_categorias.listas_id = listas.id
INNER JOIN categorias ON categorias.id = listas_categorias.categorias_id
LEFT JOIN sub_categorias ON sub_categorias.id = listas_categorias.subcategorias_id