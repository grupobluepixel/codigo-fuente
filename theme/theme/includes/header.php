<header id="navbar" class="navbar navbar-default">

    <script type="text/javascript">

            try{ace.settings.check('navbar' , 'fixed')}catch(e){}

    </script>



    <div class="navbar-container" id="navbar-container">

            <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">

                    <span class="sr-only">Toggle sidebar</span>



                    <span class="icon-bar"></span>



                    <span class="icon-bar"></span>



                    <span class="icon-bar"></span>

            </button>



            <div class="navbar-header pull-left">

                    <a href="<?= site_url() ?>" class="navbar-brand">

                            <small>
                                    <?= img('img/'.$this->db->get('ajustes')->row()->logo,'width:190px') ?>
                            </small>

                    </a> 

            </div>

            <?php if($this->user->log): ?>

            <div class="navbar-buttons navbar-header pull-right" role="navigation">

                    <ul class="nav ace-nav">

                        <li class="green">
                                    <?php 
                                        $this->db->select('denuncias.*, listas.titulo_corto as lista');
                                        $this->db->join('listas','listas.id = denuncias.listas_id');
                                        $denuncias = $this->db->get_where('denuncias',array('estado < '=>2));
                                    ?>
                                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                            <i class="ace-icon fa fa-envelope"></i>
                                            <span class="badge badge-success"><?= $denuncias->num_rows() ?></span>
                                    </a>
                                    <ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
                                            <li class="dropdown-header">
                                                    <i class="ace-icon fa fa-envelope-o"></i>
                                                    <?php if($denuncias->num_rows()==0): ?>
                                                    Sin denuncias
                                                    <?php else: ?>
                                                    <?= $denuncias->num_rows() ?> denuncias sin leer
                                                    <?php endif ?>
                                            </li>
                                            <li class="dropdown-content">
                                                    <ul class="dropdown-menu dropdown-navbar">
                                                        <?php if($denuncias->num_rows()==0): ?>
                                                            <li>Sin Denuncias</li>
                                                        <?php else: ?>
                                                            <?php foreach($denuncias->result() as $n=>$v): ?> 
                                                                <?php if($n<5): ?>
                                                                    <li><a href='<?= base_url('listas/admin/denuncias') ?>'><?= $v->lista ?></a></li>
                                                                <?php endif ?>
                                                            <?php endforeach ?>
                                                        <?php endif ?>                                                       
                                                    </ul>
                                            </li>
                                    </ul>
                            </li>



                            <li class="light-blue">



                                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">

                                            <img class="nav-user-photo" src="<?= base_url(empty($this->user->foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.$this->user->foto) ?>" alt="<?= $this->user->nombre ?>" />

                                            <span class="user-info">

                                                    <small>Bienvenido</small>

                                                    <?= $this->user->nombre ?>

                                            </span>

                                            <i class="ace-icon fa fa-caret-down"></i>

                                    </a>



                                    <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">

                                            <li>

                                                    <a href="#">

                                                            <i class="ace-icon fa fa-cog"></i>

                                                            Configuración

                                                    </a>

                                            </li>



                                            <li>

                                                    <a href="<?= base_url('seguridad/perfil/edit/'.$this->user->id) ?>">

                                                            <i class="ace-icon fa fa-user"></i>

                                                            Perfil

                                                    </a>

                                            </li>

                                            <li>

                                                    <a href="<?= base_url('main/unlog') ?>">

                                                            <i class="ace-icon fa fa-power-off"></i>

                                                            Salir

                                                    </a>

                                            </li>

                                    </ul>

                            </li>

                    </ul>

            </div>

            <?php endif ?>

    </div><!-- /.navbar-container -->

</header>

