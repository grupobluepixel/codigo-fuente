<div class="panel panel-default">
	<div class="panel-heading">
		<h1 class="panel-title">Filtros por fecha</h1>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<input class="date form-control" type="text" id="desde" name="desde" placeholder="Desde dd/mm/YYYY">
			</div>
			<div class="col-xs-12 col-sm-6">
				<input class="date form-control" type="text" id="hasta" name="hasta" placeholder="Hasta dd/mm/YYYY">
			</div>
		</div>
	</div>
</div>

<?= $output ?>
<?php $this->load->view('predesign/datepicker') ?>
<script>
	$(".date").datepicker({
        Format: "dd/mm/yy",
        showButtonPanel: true,
        changeMonth: true,
        changeYear: true
    });
	$(".date").on('change',function(){
		$(".filtering_form").append('<input type="hidden" name="desde" value="'+$("#desde").val()+'">');
		$(".filtering_form").append('<input type="hidden" name="hasta" value="'+$("#hasta").val()+'">');
		$(".filtering_form").submit();
	});
</script>