<!-- Modal Foto Publicar Lista -->
<div class="modal fade" id="seleccionar-foto-lista" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close text-right" data-dismiss="modal" aria-label="Close" style="padding:2px 10px;"><i class="material-icons">clear</i></button>
            <div class="modal-header center-block text-center">
                <h5 class="modal-title"><img src="<?=  base_url() ?>theme/theme/views/assets/img/logo-snacklist-black.png" alt="Logo Snacklist" class="img-responsive center-block"></h5>
            </div>
            <div class="modal-body">

                <!--
                <div class="col-sm-12 contenedor-tarjeta-redeem">
                    <div class="col-sm-1">1</div>
                    <div class="col-sm-3">
                        <img src="<?=  base_url() ?>theme/theme/assets/img/Perfiles/perfil.jpg" id="fileAdjuntLista"  alt="Perfil Snacktrend" class="center-block img-responsive">
                    </div>
                    <div class="col-sm-8">
                        <div class="col-sm-12">
                            <span class="bmd-form-group">
                                <input class="form-control" id="input-publicar-lista" name="detalles[0][nombre]" placeholder="Nombre del elemento" type="text">
                            </span>
                        </div>
                        <div class="col-sm-12">
                            <span class="bmd-form-group">
                                <input class="form-control margin-login-input" id="input-publicar-lista" name="detalles[0][nombre]" placeholder="Imagen / video por:" type="text">
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-12 text-center">
                        *No olvides dar crédito al autor de la fotografía
                    </div>
                </div>-->

                <div class="col-sm-12">

                    <div class="profile-tabs">
                        <ul class="busquedaNav nav nav-pills nav-pills-icons justify-content-center" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active show" href="#imagen" role="tab" data-toggle="tab" aria-selected="true">
                                  <b>Carga una imagen</b>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#google" role="tab" data-toggle="tab" aria-selected="false">
                                  <b>Desde Google</b>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#youtube" role="tab" data-toggle="tab" aria-selected="false">
                                  <b>Desde Youtube</b>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#giphy" role="tab" data-toggle="tab" aria-selected="false">
                                  <b>Desde Giphy</b>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content">

                        <div class="tab-pane work active show" id="imagen">
                            <div class="col-sm-12 contenedor-imagenes-modal">
                              <div class="col-sm-12 center-block text-center">
                                    <div><b>Carga una imagen (jpg, png o gif)</b></div>
                                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                        <span class="btn btn-file">
                                            <span class="fileinput-new">
                                                <div class="center-block" id="img-login-registro"><i class="material-icons">jpg</i></div>
                                            </span>
                                            <span class="fileinput-exists">Change</span>
                                            <input name="" id="uploadImageLista" type="file">
                                        </span>
                                        <div class="progress" id="progressUploadFileLista" style="display: none;">
                                            <div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
                                        </div>
                                        <a href="#" id="removeUploadFileLista" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput">
                                            <i class="fa fa-times"></i> Remove
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane text-center connections contenedor-modal-buscar" id="google">
                            <img src="<?=  base_url() ?>theme/theme/views/assets/img/logo-google.png" id="" alt="Snacklist" class="center-block img-responsive">
                            <form action="">
                                <div class="search">
                                    <input type="text" name="q" id="buscarImagenGoogle" class="busquedaAdjunto form-control" placeholder="Buscar Imagen" />
                                    <button type="submit" class="btn btn-primary">Buscar</button>
                                </div>
                            </form>
                            <div class="contendor-buscador-imagenes">
                                <div id="buscarImagenGoogleResponse"></div>
                            </div>
                        </div>

                        <div class="tab-pane text-center gallery contenedor-modal-buscar" id="youtube">
                            <img src="<?=  base_url() ?>theme/theme/views/assets/img/logo-youtube.png" id="" alt="Snacklist" class="center-block img-responsive">
                            <form action="">
                                <div class="search">
                                    <input type="text" name="q" id="buscarYoutubeVideo" class="busquedaAdjunto form-control" placeholder="Buscar Video" />
                                    <button type="submit" class="btn btn-primary">Buscar</button>
                                </div>
                            </form>
                            <div class="contendor-buscador-imagenes">
                                <div id="buscarYoutubeVideoResponse"></div>
                            </div>
                        </div>

                        <div class="tab-pane text-center gallery contenedor-modal-buscar" id="giphy">
                            <img src="<?=  base_url() ?>theme/theme/views/assets/img/logo-giphy.png" id="" alt="Snacklist" class="center-block img-responsive">
                            <form action="">
                                <div class="search">
                                    <input type="text" name="q" id="buscarImagenGiphy" class="busquedaAdjunto form-control" placeholder="Buscar Giphy" />
                                    <button type="submit" class="btn btn-primary">Buscar</button>
                                </div>
                            </form>
                            <div class="contendor-buscador-imagenes">
                                <div id="buscarImagenGifResponse"></div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><b>Cancelar</b></button>
                <button type="button" class="btn btn-success" data-dismiss="modal"><b>Guardar</b></button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Foto Publicar Lista -->

<!-- Modal Eliminar  Lista -->
<div class="modal fade" id="eliminar-lista" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close text-right" data-dismiss="modal" aria-label="Close" style="padding:2px 10px;"><i class="material-icons">clear</i></button>
            <div class="modal-header center-block text-center">
                <h5 class="modal-title"><img src="<?=  base_url() ?>theme/theme/views/assets/img/logo-snacklist-black.png" alt="Logo Snacklist" class="img-responsive center-block"></h5>
            </div>
            <div class="modal-body">
                <div class="col-sm-12 center-block text-center">
                    <div><b>¿Seguro de eliminar esta lista?</b></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><b>Cancelar</b></button>
                <button type="button" class="btn btn-success"><b>Eliminar</b></button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Eliminar Lista -->

<!-- Modal Reportar Lista -->
<div class="modal fade" id="reportar-lista" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <form action="" onsubmit="return reportar(this)">
                <button type="button" class="close text-right" data-dismiss="modal" aria-label="Close" style="padding:2px 10px;"><i class="material-icons">clear</i></button>
                <div class="modal-header center-block text-center">
                    <h5 class="modal-title"><img src="<?=  base_url() ?>theme/theme/views/assets/img/logo-snacklist-black.png" alt="Logo Snacklist" class="img-responsive center-block"></h5>
                </div>
                <div class="modal-body">
                    <div class="col-sm-12 center-block text-center">
                        <div><b>Si deseas denunciar este comentario de forma anónima, por favor indícanos la razón y lo revisaremos personalmente.</b></div>
                        <div class="reportar-modal">
                            <div class="form-check margin-bottom-modal">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="razon" id="exampleRadios1" value="El contenido es inapropiado, explícito, publicitario u ofensivo" type="radio"> El contenido es inapropiado, explícito, publicitario u ofensivo
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                            <div class="form-check margin-bottom-modal">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="razon" id="exampleRadios2" value="El contenido es deliberadamente malintencionado" checked="" type="radio">El contenido es deliberadamente malintencionado
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                            <div class="form-check margin-bottom-modal">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="razon" id="exampleRadios2" value="El contenido es falso o contiene información falsa" checked="" type="radio">El contenido es falso o contiene información falsa
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="listas_id" value="<?= @$lista->id ?>">
                    <div class="denunciaResult"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><b>Cancelar</b></button>
                    <button type="submit" class="btn btn-success"><b>Enviar</b></button>
                </div>
            </form>

        </div>
    </div>
</div>
<!-- Modal Reportar Lista -->

<!-- Modal Registro -->
<div class="modal fade" id="login" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <!-- <div class="row text-center titulo-modal center-block">Regístrate</div>-->
        <div class="modal-content">
            <button type="button" class="close text-right" data-dismiss="modal" aria-label="Close" style="padding:2px 10px;"><i class="material-icons">clear</i></button>
            <div class="modal-body">
                <form id="formularioRegistro" action="<?= base_url('registro/index/add') ?>" onsubmit="return registrar(this)" method="post">
                    <div class="col-sm-12 center-block text-center">

                        <div class="col-sm-12">
                            <div><b>Crea una cuenta</b></div>
                            <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                <span class="btn btn-file">
                                    <span class="fileinput-new">
                                        <div class="center-block" id="img-login-registro"><i class="material-icons">camera_alt</i></div>
                                    </span>
                                    <!--
                                    <div class="col-sm-12"><span class="fileinput-exists">Change</span></div>
                                    <div class="col-sm-12"><span id="progresoSubida" style="color:black;"></span></div>-->
                                    <div class="col-sm-12"><img id="imagePreviewRegistro" alt="" src="" style="display: none; width:15%;"></div>
                                    <input name="s66c9eed1" id="fotoFile" class="registrofoto" type="file">
                                </span>
                                <a href="#" id="borrarFotoRegistro" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <input class="form-control margin-login-input" id="prueba" name="nickname" placeholder="Usuario" type="text" maxlength="20">
                            <input class="form-control margin-login-input" id="prueba" name="email" placeholder="Correo" type="email">
                            <input class="form-control margin-login-input" id="prueba" name="password" placeholder="Contraseña" type="password" maxlength="20">
                            <input class="form-control margin-login-input" id="prueba" name="password2" placeholder="Confirmar contraseña" type="password" maxlength="20">

                            <div class="genero-login">
                                <div class="form-check margin-bottom-modal genero-modal-registro">
                                    <label class="form-check-label">
                                        <input class="form-check-input" name="genero" id="exampleRadios1" value="Masculino" type="radio">Hombre
                                        <span class="circle"><span class="check"></span></span>
                                    </label>
                                </div>
                                <div class="form-check margin-bottom-modal genero-modal-registro">
                                    <label class="form-check-label">
                                        <input class="form-check-input" name="genero" id="exampleRadios2" value="Femenino" type="radio">Mujer
                                        <span class="circle"><span class="check"></span></span>
                                    </label>
                                </div>
                            </div>

                            <input class="form-control margin-login-input datepicker" id="cumple" value="<?= date("d/m/Y") ?>" name="fecha_cumpleanos" placeholder="Fecha de cumpleaños" style="" type="text">
                            <input class="form-control margin-login-input" id="prueba" placeholder="Código Postal" name="cp" type="tel" maxlength="8">
                            <input type="hidden" name="foto" id="fotoFieldRegistro" value="">
                        </div>
                        <div class="col-sm-12 texto-aviso-login">
                          *Al crear una cuenta aceptas el uso de cookies, los
                          <a href="<?= base_url() ?>terminos.html">Términos y condiciones</a> y el
                          <a href="<?= base_url() ?>aviso.html">Aviso de privacidad.</a>
                        </div>
                        <div class="col-xs-12" id="registroResult"></div>
                        <div class="col-sm-12 captcha" style="width: 100%; height: 78px;">
                            <div class="g-recaptcha" style="width: 100%; height: 78px;" data-sitekey="6LdmUHEUAAAAAO3eZK1pmJEAO1k6F5Qg8CTwCSTu"></div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-12 padding0">
                                <button type="submit" class="btn btn-primary" style="width:100%; margin-bottom:10px;">
                                    <b>Crear cuenta</b>
                                </button>
                            </div>
                            <div class="col-sm-12 ya-tengo-cuenta">
                                <a href="#" onclick="$('#login').modal('toggle')" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#iniciar-sesion">Ya tengo cuenta</a>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal Registro -->

<!-- Modal Contraseña -->
<div class="modal fade" id="recuperar-contraseña" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close text-right" data-dismiss="modal" aria-label="Close" style="padding:2px 10px;"><i class="material-icons">clear</i></button>
            <div class="modal-header center-block text-center">
                <h5 class="modal-title"><img src="<?=  base_url() ?>theme/theme/views/assets/img/logo-snacklist-black.png" alt="Logo Snacklist" class="img-responsive center-block"></h5>
            </div>
            <div class="modal-body">
                <div class="col-sm-12 center-block text-center">
                    <div class="col-sm-12">
                        <input class="form-control margin-login-input" id="prueba" placeholder="Correo" type="email">
                    </div>
                    <div class="col-sm-12" style="padding-left:0px; padding-right:0px">
                        <div class="col-sm-6 pull-right" style="padding-left:0px; padding-right:0px">
                            <a href="#" class="btn btn-primary"><b>Recuperar mi contraseña</b></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Contraseña -->


<!-- Modal Login -->
<div class="modal fade" id="iniciar-sesion" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close text-right" data-dismiss="modal" aria-label="Close" style="padding:2px 10px;"><i class="material-icons">clear</i></button>
            <div class="modal-header center-block text-center">
                <h5 class="modal-title"><img src="<?=  base_url() ?>theme/theme/views/assets/img/logo-snacklist-black.png" alt="Logo Snacklist" class="img-responsive center-block"></h5>
            </div>
            <div class="modal-body">
                <form id="formularioRegistro" action="<?= base_url('main/loginUser') ?>" onsubmit="return loginShort(this)" method="post">
                    <div class="col-sm-12 center-block text-center">

                        <div class="col-sm-12">
                            <input class="form-control margin-login-input" id="prueba" name="email" placeholder="Correo" type="email">
                            <input class="form-control margin-login-input" id="prueba" name="pass" placeholder="Contraseña" type="password">
                        </div>
                        <div class="col-sm-12" id="loginShortResult"></div>
                        <div class="col-sm-12">
                            <div class="col-sm-12 padding0">
                                <button type="submit" class="btn btn-primary" style="width:100%; margin-bottom:10px;">
                                    <b>Iniciar sesión</b>
                                </button>
                            </div>
                            <a href="#" onclick="$('#iniciar-sesion').modal('toggle')" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#login">No tengo cuenta</a>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal Login -->



<!-- Modal Niveles de usuario -->
<div class="modal fade" id="niveles-usuario" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <!-- <div class="row text-center titulo-modal center-block">Regístrate</div>-->
        <div class="modal-content">
            <div class="fondo-gradient-azul texto-blanco text-center contenedor-niveles-modal">

                <div class="col-sm-12 titulo-modal-snacks"><b>Snacks</b></div>

                <div class="col-md-12">
                    <div class="subtitulo-modal-snacks"><b>1-1000 snacks</b></div>
                    <div class="nombre-perfil-editar center-block" style="width:60%;">
                        <img src="<?=  base_url() ?>theme/theme/views/assets/img/Iconos/001_dulce.png" class="img-responsive center-block img-modal-niveles">
                        <div class="imagen-editar text-center" style="width:100%;">
                            <div class="nivel-editar">Nivel 1</div>
                            <div class="titulo-editar"><b>Lonchero</b></div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="col-sm-6 borde-separacion-modal"></div>
                    <div class="col-sm-6"></div>
                </div>

                <div class="col-md-12">
                    <div class="subtitulo-modal-snacks"><b>1001-3000 snacks</b></div>
                    <div class="nombre-perfil-editar center-block" style="width:60%;">
                        <img src="<?=  base_url() ?>theme/theme/views/assets/img/Iconos/002_cuernito.png" class="img-responsive center-block img-modal-niveles">
                        <div class="imagen-editar text-center" style="width:100%;">
                            <div class="nivel-editar">Nivel 2</div>
                            <div class="titulo-editar"><b>Taquero</b></div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="col-sm-6 borde-separacion-modal"></div>
                    <div class="col-sm-6"></div>
                </div>

                <div class="col-md-12">
                    <div class="subtitulo-modal-snacks"><b>3001-6000 snacks</b></div>
                    <div class="nombre-perfil-editar center-block" style="width:60%;">
                        <img src="<?=  base_url() ?>theme/theme/views/assets/img/Iconos/003_dona.png" class="img-responsive center-block img-modal-niveles">
                        <div class="imagen-editar text-center" style="width:100%;">
                            <div class="nivel-editar">Nivel 3</div>
                            <div class="titulo-editar"><b>Come Sushi</b></div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="col-sm-6 borde-separacion-modal"></div>
                    <div class="col-sm-6"></div>
                </div>

                <div class="col-md-12">
                    <div class="subtitulo-modal-snacks"><b>6001-10,000 snacks</b></div>
                    <div class="nombre-perfil-editar center-block" style="width:60%;">
                        <img src="<?=  base_url() ?>theme/theme/views/assets/img/Iconos/004_nachos.png" class="img-responsive center-block img-modal-niveles">
                        <div class="imagen-editar text-center" style="width:100%;">
                            <div class="nivel-editar">Nivel 4</div>
                            <div class="titulo-editar"><b>Hipster Gourmet</b></div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="col-sm-6 borde-separacion-modal"></div>
                    <div class="col-sm-6"></div>
                </div>

                <div class="col-md-12">
                    <div class="subtitulo-modal-snacks"><b>10,001 snacks</b></div>
                    <div class="nombre-perfil-editar center-block" style="width:60%;">
                        <img src="<?=  base_url() ?>theme/theme/views/assets/img/Iconos/005_taco.png" class="img-responsive center-block img-modal-niveles">
                        <div class="imagen-editar text-center" style="width:100%;">
                            <div class="nivel-editar">Nivel 5</div>
                            <div class="titulo-editar"><b>Bon Vivant</b></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- Modal Niveles de usuario -->

<!-- Modal Listas VS Listas -->
<div class="modal fade" id="listavslista" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="row text-center titulo-modal center-block">Denunciar anónimamente</div>
        <div class="modal-content">

            <button type="button" class="close text-right" data-dismiss="modal" aria-label="Close" style="padding:2px 10px;"><i class="material-icons">clear</i></button>
            <div class="modal-header center-block text-center">
                <h5 class="modal-title"><img src="<?=  base_url() ?>theme/theme/views/assets/img/logo-snacklist-black.png" alt="Logo Snacklist" class="img-responsive center-block"></h5>
            </div>

            <div class="modal-body">


                




            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal"><b>Cerrar</b></button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Listas VS Listas -->

<!-- Modal Listas VS Listas -->
<div class="modal fade" id="verAdjuntoModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="row text-center titulo-modal center-block">Denunciar anónimamente</div>
        <div class="modal-content">
            <button type="button" class="close text-right" data-dismiss="modal" aria-label="Close" style="padding:2px 10px;"><i class="material-icons">clear</i></button>
            <div class="modal-header center-block text-center">
                <h5 class="modal-title"><img src="<?=  base_url() ?>theme/theme/views/assets/img/logo-snacklist-black.png" alt="Logo Snacklist" class="img-responsive center-block"></h5>
            </div>
            <div class="modal-body">


                COntenido




            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal"><b>Cerrar</b></button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Listas VS Listas -->


<script>

    $("#fotoFile").on('change',function(){
        var f = new FormData(document.getElementById('formularioRegistro'));
        $.ajax({
            url: '<?= base_url('registro/index/upload_file/foto') ?>',
            data: f,
            context: document.body,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                //Download progress
                xhr.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        console.log(Math.round(percentComplete * 100) + "%");
                        $("#progresoSubida").html(Math.round(percentComplete * 100) + "%");
                    }
                }, false);

                return xhr;
            },
            success:function(data){
                data = JSON.parse(data);
                var name = data.files[0].name;
                console.log(name);
                $("#fotoFieldRegistro").val(name);
                $("#imagePreviewRegistro").attr('src',URL+'img/fotos/'+name);
                $("#imagePreviewRegistro").show();
            }
        });

    });

    $("#borrarFotoRegistro").on('click',function(){
        $.get('<?= base_url('registro/index/delete_file/foto/') ?>/'+$("#fotoFieldRegistro").val(),{},function(data){
            $("#fotoFieldRegistro").val('');
            $("#imagePreviewRegistro").hide();
        });
    });

    function reportar(f){
        var f = new FormData(f);
        $(".denunciaResult").removeClass('alert alert-success alert-danger');
        insertarUser('listas/listados/denuncias/insert',f,function(data){
            if(data.success){
                $(".denunciaResult").addClass('alert alert-success').html('Hemos enviado su solicitud');
            }else{
                $(".denunciaResult").addClass('alert alert-danger').html('Ocurrio un error al añadir');
            }
        },'.denunciaResult');

        return false;
    }


</script>
