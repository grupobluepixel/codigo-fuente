<div class="container-fluid margin-secciones-menu-top">
    <div class="container contenedor-ads">
        <div class="row text-center">Google ADS</div>
    </div>
</div>

<div class="container-fluid contenedor-snackteam fondo-blanco">
  <div class="container">
    <div class="row text-center contenedor-perfil-redeem">
      <!--
      <img src="<?= base_url() ?>theme/theme/assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
      <h3><b>Snackteam</b></h3>
      -->
      <div class="row">
        <div class="col-sm-6 padding0 breadcumb-redeem">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <?php if($lista->categorias->num_rows()>0): ?>
                <li class="breadcrumb-item"><a href="<?= base_url('categoria/'.toUrl($lista->categorias->row()->id.'-'.$lista->categorias->row()->nombre)) ?>"><b><?= @$lista->categorias->row()->nombre ?></b></a></li>
              <?php endif ?>
              <?php if($lista->subcategorias->num_rows()>0): ?>
                <li class="breadcrumb-item"><a href="<?= base_url('subcategoria/'.toUrl($lista->subcategorias->row()->id.'-'.$lista->subcategorias->row()->nombre)) ?>"><b><?= @$lista->subcategorias->row()->nombre ?></b></a></li>
              <?php endif ?>
            </ol>
          </nav>
        </div>
        <div class="col-sm-6 padding0 reportar-redeem" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#reportar-lista">
          <a href="#"><i class="material-icons">flag</i><b>Reportar Lista</b></a>
        </div>
      </div>
      <div class="row titulo-redeem texto-negro">
        <div class="col-sm-12">
          <h2><?= $lista->titulo ?></h2>
        </div>
      </div>
    </div>
    <div class="row text-center contenedor-votos-redeem">
      <div class="col-sm-3 borde-redeem"><b><?= $lista->vistas ?> Vistas</b></div>
      <div class="col-sm-3 borde-redeem"><b><?= $lista->votos ?> Votos</b></div>
      <div class="col-sm-3 borde-redeem"><b><?= $lista->veces_ordenado ?> Reordenadas</b></div>
      <div class="col-sm-3">
        <div class="texto-comparte-redeem"><b>Comparte esta lista:</b></div>
        <div class="redes-redeem">
          <?php
            $miniatura = $this->querys->get_foto(@$this->db->get_where('listas_detalles',array('listas_id'=>$lista->id))->row()->adjunto);
            $lista->link = base_url('lista/'.toUrl($lista->id.'-'.$lista->titulo_corto));
          ?>
          <a class="shareFacebookLink" href="javascript:shareFacebook(<?= $lista->id ?>,'<?= $lista->titulo_corto ?>','<?= $lista->link ?>','<?= $miniatura ?>')"><i class="fa fa-facebook-official"></i></a>
          <?php if(empty($_SESSION['user'])): ?>
          <a href="javascript:iniciarLogin()"><i class="fa fa-twitter"></i></a>
          <?php else: ?>
            <a data-id="<?= $lista->id ?>" href="http://twitter.com/intent/tweet?url=<?= $lista->link ?>;via=snacklist&text=The+Snackilst:+Listas+creadas+y+votadas+por+todos."><i class="fa fa-twitter"></i></a>
          <?php endif ?>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid contenedor-reordenar">
  <div class="container">
    <div class="row text-center">
      <div class="col-md-8 col-md-offset-2">
        <ul class="nav nav-pills contenedor-nav-redeem padding0">
          <li class="nav-item" style="width: 49%;"><a class="nav-link" href="#votar" data-toggle="tab" id="menu-reordenar"><b>Votar</b></a></li>
          <li class="nav-item" style="width: 49%;"><a class="nav-link active show" href="#reordenar" data-toggle="tab" id="menu-reordenar"><b>Reordenar</b></a></li>
        </ul>
        <div class="tab-content tab-space">
          <!-- Contenedor Votar -->
          <div class="tab-pane" id="votar">
            <div class="col-sm-12 texto-intrucciones">
              <p>
                <b>Votar</b><br>
                Con la acción de votar das click en +1 o -1 en cada elemento que quieras. Asi los elementos pueden
                cambiar su posición dentro de la lista. Si coincide la sumatoria de dos elementos, el que estaba
                arriba prevalece hasta arriba.
              </p>
            </div>
            <!-- TOP 10 -->
            <div class="col-sm-12 contenedor-tabs-redeem fondo-azul-claro">
              <!-- Tarjeta redeem -->


              <?php foreach($lista->detalles->result() as $n=>$l): ?>
                  <div class="col-sm-12 contenedor-tarjeta-redeem">
                    <div class="col-sm-1 numero-reordenar2"><?= $l->posicion ?></div>
                    <div class="col-sm-2">
                      <img src="<?= $this->querys->get_foto($l->adjunto) ?>" alt="<?= $l->nombre ?>" class="center-block img-responsive img-redeem">
                    </div>
                    <div class="col-sm-6 titulo-reddem">
                      <h2>
                      <b><?= $l->nombre ?></b>
                      <br>
                      <span>Foto: <?= $l->autor ?></span>
                      </h2>
                    </div>
                    <div class="col-sm-3 votaciones votar<?= $l->id ?>">
                      <?php if(empty($_SESSION['user']) || $lista->user_id!=$_SESSION['user']): ?>
                      <a href="javascript:minus(<?= $l->id ?>)" class="reordenar-redeem numero-menos">-1</a>
                      <a href="javascript:plus(<?= $l->id ?>)" class="reordenar-redeem numero-mas">+1</a>
                      <?php endif ?>
                    </div>
                  </div>
              <?php endforeach ?>

            </div>
          </div>
          <!-- Contenedor Reordenar -->
          <div class="tab-pane  active show" id="reordenar">

            <form action="" onsubmit="return guardarLista(this)" method="post">
                    <div class="col-sm-12 texto-intrucciones">
                      <p>
                        <b>Reordenar</b><br>
                        Con la acción de reordenar puedes sumar hasta 55 puntos a la lista existente. Cada vez que reordenas
                        una lista de 10 elementos, sumas un total de 55 puntos a la lista. Estos puntos se suman a los ya acumulados
                        para determinar la posición del elemento.
                      </p>
                    </div>
                    <!-- TOP 10 -->
                    <div class="col-sm-12 contenedor-tabs-redeem">
                      <div class='parent'>
                        <div class='wrapper'>
                          <div id='left-rollbacks' class='container-drag'>
                            <?php foreach($lista->detalles->result() as $n=>$l): ?>
                                
                                    <!-- Tarjeta redeem -->
                                    <div class="col-sm-12 contenedor-tarjeta-redeem <?= $n<10?'lista-primeros-lugares':'' ?>">
                                      <div class="col-sm-1 numero-votar">
                                        <i class="material-icons">arrow_drop_up</i><br>
                                        <span class="indice"><?= $l->posicion ?></span><br>
                                        <i class="material-icons">arrow_drop_down</i>
                                      </div>
                                      <div class="col-sm-2">
                                        <img src="<?= $this->querys->get_foto($l->adjunto) ?>" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem">
                                        <a href="#" class="adjuntoPreview" title="Preview">
                                            <div class="ver-preview"><i class="material-icons">remove_red_eye</i></div>
                                        </a>
                                      </div>
                                      <div class="col-sm-7 titulo-reddem">
                                        <h2>
                                        <b><?= $l->nombre ?></b>
                                        <br>
                                        <span>Foto: <?= $l->autor ?></span>
                                        </h2>
                                      </div>
                                      <div class="col-sm-1 icon-redeem">
                                          <i class="material-icons">more_vert</i>
                                      </div>
                                      <div class="col-sm-1 icon-redeem remove2">
                                          <i class="material-icons">delete_forever</i>
                                      </div>
                                      <input type="hidden" name="detalles[<?= $n ?>][nombre]" value="<?= $l->nombre ?>" data-name="nombre">
                                      <input type="hidden" name="detalles[<?= $n ?>][autor]" value="<?= $l->autor ?>" data-name="autor">
                                      <input type="hidden" class="adjunto" name="detalles[<?= $n ?>][adjunto]" value="<?= $l->adjunto ?>" data-name="adjunto">
                                      <input type="hidden" class="tipo_adjunto" name="detalles[<?= $n ?>][tipo_adjunto]" value="<?= $l->tipo_adjunto ?>" data-name="tipo_adjunto">
                                      <input type="hidden" name="detalles[<?= $n ?>][id]" value="<?= $l->id ?>" data-name="id">
                                    </div>
                                
                            <?php endforeach ?>
                            
                              <?php for($i=$lista->detalles->num_rows()+1;$i<=10;$i++): ?>
                                
                                  <div class="col-sm-12 contenedor-tarjeta-redeem <?= $i==10?'participantRow':'' ?> <?= $i<=10?'lista-primeros-lugares':'' ?>">
                                    <div class="col-sm-1 numero-reordenar">
                                      <i class="material-icons">arrow_drop_up</i><br>
                                      <span class="indice"><?= $i ?></span>
                                      <i class="material-icons">arrow_drop_down</i>
                                    </div>
                                    <div class="col-sm-2">
                                      <div rel="tooltip" onclick="setInputImagenLista(this)" data-original-title="" data-toggle="modal" data-target="#seleccionar-foto-lista" title="Agregar una imagen, video o gif">
                                        <img src="<?= $this->querys->get_foto('') ?>" alt="Nuevo elemento" class="center-block img-responsive img-redeem">
                                        <a href="#" class="adjuntoPreview" title="Preview">
                                            <div class="ver-preview"><i class="material-icons">remove_red_eye</i></div>
                                        </a>
                                      </div>
                                    </div>
                                    <div class="col-sm-7 titulo-reddem">
                                        <div class="col-sm-12">
                                            <input class="form-control margin-login-input" name="detalles[<?= $i ?>][nombre]" type="text" value="" data-name="nombre">
                                        </div>

                                        <div class="col-sm-12">
                                            <input class="form-control margin-login-input" id="input-publicar-lista"  name="detalles[<?= $i ?>][autor]" data-name="autor" placeholder="Autor de la imagen" type="text" value="">
                                            <input type="hidden" id="adjunto<?= $i ?>" class="adjunto" name="detalles[<?= $i ?>][adjunto]" data-name="adjunto" value="">
                                            <input type="hidden" id="tipo_adjunto<?= $i ?>" class="tipo_adjunto" name="detalles[<?= $i ?>][tipo_adjunto]" data-name="tipo_adjunto" value="">
                                        </div>
                                    </div>
                                     <div class="col-sm-1 icon-redeem">
                                          <i class="material-icons">more_vert</i>
                                      </div>
                                      <div class="col-sm-1 icon-redeem remove">
                                          <i class="material-icons">delete_forever</i>
                                      </div>
                                  </div>
                              <?php endfor; ?>
                              <?php if($lista->detalles->num_rows()>9): $i = $lista->detalles->num_rows(); ?>
                                <div class="col-sm-12 contenedor-tarjeta-redeem participantRow " style="<?= $i>=10?'display:none':'' ?>">
                                    <div class="col-sm-1 numero-reordenar">
                                      <i class="material-icons">arrow_drop_up</i><br>
                                      <span class="indice"><?= $i ?></span>
                                      <i class="material-icons">arrow_drop_down</i>
                                    </div>
                                    <div class="col-sm-2">
                                      <div rel="tooltip" onclick="setInputImagenLista(this)" data-original-title="" data-toggle="modal" data-target="#seleccionar-foto-lista" title="Agregar una imagen, video o gif">
                                        <img src="<?= $this->querys->get_foto('') ?>" alt="Nuevo elemento" class="center-block img-responsive img-redeem">
                                    </div>
                                    </div>
                                    <div class="col-sm-7 titulo-reddem">
                                        <div class="col-sm-12">
                                            <input class="form-control margin-login-input" name="detalles[<?= $i ?>][nombre]" type="text" value="" data-name="nombre">
                                        </div>

                                        <div class="col-sm-12">
                                            <input class="form-control margin-login-input" id="input-publicar-lista"  name="detalles[<?= $i ?>][autor]" data-name="autor" placeholder="Autor de la imagen" type="text" value="">
                                            <input type="hidden" id="adjunto<?= $i ?>" class="adjunto" name="detalles[<?= $i ?>][adjunto]" data-name="adjunto" value="">
                                            <input type="hidden" id="tipo_adjunto<?= $i ?>" class="tipo_adjunto" name="detalles[<?= $i ?>][tipo_adjunto]" data-name="tipo_adjunto" value="">
                                        </div>
                                    </div>
                                     <div class="col-sm-1 icon-redeem">
                                          <i class="material-icons">more_vert</i>
                                      </div>
                                      <div class="col-sm-1 icon-redeem remove">
                                          <i class="material-icons">delete_forever</i>
                                      </div>
                                  </div>
                              <?php endif ?>





                            </div><!-- Termina div Drag & Drop -->
                          </div>
                        </div>
                      </div>





                      <!-- Posiciones -->
                      <div class="col-sm-12 contenedor-tabs-redeem">
                            <?php foreach($lista->categorias->result() as $c): ?>
                                <input type="hidden" name="categorias_id[]" value="<?= $c->categorias_id ?>">
                                <input type="hidden" name="sub_categorias_id[]" value="<?= $c->subcategorias_id ?>">
                                <input type="hidden" name="userordenado" value="<?= @$this->user->id ?>">
                            <?php endforeach ?>
                      </div>
                      <div class="col-sm-12">
                          <div id="result"></div>
                      </div>

                      <?php if(empty($_SESSION['user']) || $this->user->id!=$lista->user_id): ?>
                          <div class="col-sm-12 contenedor-tabs-redeem" style="display: flex; font-size: 12px;">
                            <button class="add" type="button" style="background-color: transparent; border: 0px; width: 100%;">
                              <div class="col-sm-7 text-center btn-menu-top btn-general btn-derecha pull-left"><b>Agregar elemento</b></div>
                            </button>
                            <button type="submit">
                              <div class="col-sm-5 text-center btn-menu-top btn-general btn-derecha pull-right"><b>Guardar</b></div>
                            </button>
                          </div>
                      <?php endif ?>


                    </div>




              </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid fondo-blanco contendor-compartir">
    <div class="container">
      <div class="row text-center">
        <div class="col-sm-12">
          <div class="btn-compartir-lista">
            <a href="javascript:shareFacebook(<?= $lista->id ?>,'<?= $lista->titulo_corto ?>','<?= $lista->link ?>','<?= $miniatura ?>')">Compartir <i class="material-icons">share</i></a>
          </div>
          <div class="fb-comments" data-href="<?= $lista->link ?>" data-numposts="5"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid fondo-azul-claro">
    <div class="container">
      <div class="row text-center">
        <!-- Recomendaciones -->
        <div class="col-sm-12">
          <?php include('recomendaciones.php');?>
        </div>
      </div>
    </div>
  </div>

  
  <div id="emptyRows" style="display: none;">
    <div class="col-sm-12 contenedor-tarjeta-redeem participantRow">
      <div class="col-sm-1 numero-reordenar">
        <i class="material-icons">arrow_drop_up</i><br>
        <span class="indice"><?= $i ?></span>
        <i class="material-icons">arrow_drop_down</i>
      </div>
      <div class="col-sm-2">
        <div rel="tooltip" onclick="setInputImagenLista(this)" data-original-title="" data-toggle="modal" data-target="#seleccionar-foto-lista" title="Agregar una imagen, video o gif">
          <img src="<?= $this->querys->get_foto('') ?>" alt="Nuevo elemento" class="center-block img-responsive img-redeem">
      </div>
      </div>
      <div class="col-sm-7 titulo-reddem">
          <div class="col-sm-12">
              <input class="form-control margin-login-input" name="detalles[<?= $i ?>][nombre]" type="text" value="" data-name="nombre">
          </div>

          <div class="col-sm-12">
              <input class="form-control margin-login-input" id="input-publicar-lista"  name="detalles[<?= $i ?>][autor]" data-name="autor" placeholder="Autor de la imagen" type="text" value="">
              <input type="hidden" id="adjunto<?= $i ?>" class="adjunto" name="detalles[<?= $i ?>][adjunto]" data-name="adjunto" value="">
              <input type="hidden" id="tipo_adjunto<?= $i ?>" class="tipo_adjunto" name="detalles[<?= $i ?>][tipo_adjunto]" data-name="tipo_adjunto" value="">
          </div>
      </div>
       <div class="col-sm-1 icon-redeem">
            <i class="material-icons">more_vert</i>
        </div>
        <div class="col-sm-1 icon-redeem remove">
            <i class="material-icons">delete_forever</i>
        </div>
    </div>
  </div>

  <!-- Footer -->
  <footer class="container-fluid footer contenedor-footer">
    <?php include('footer.php');?>
  </footer>
  <!-- Mapa de sitio -->
  <footer class="container-fluid contenedor-mapa-sito-footer">
    <?php include('footer-mapa.php');?>
  </footer>
  <!-- Modales -->
  <?php include('modales.php');?>
  <!-- Librerias -->
  <?php include('librerias.php');?>
  <script>
  var votos = JSON.parse(localStorage.votos);
  for(var i in votos){
  if($(".votar"+votos[i]).length>0){
  $(".votar"+votos[i]).find('.numero-menos').removeClass('numero-menos');
  $(".votar"+votos[i]).find('.numero-mas').removeClass('numero-mas');
  $(".votar"+votos[i]).find('a').addClass('numero-bloqueado');
  $(".votar"+votos[i]).find('a').attr('href','#');
  }
  }
  $(".votaciones").show();
  function plus(id){
  <?php if(empty($_SESSION['user'])): ?>
      $("#iniciar-sesion").modal('toggle');
  <?php else: ?>
      remoteConnection('listas/frontend/plus/'+id,null,function(data){
      $(".votar"+id).find('.numero-menos').removeClass('numero-menos');
      $(".votar"+id).find('.numero-mas').removeClass('numero-mas');
      $(".votar"+id).find('a').addClass('numero-bloqueado');
      $(".votar"+id).find('a').attr('href','#');
      var votos = JSON.parse(localStorage.votos);
      votos.push(id);
      localStorage.votos = JSON.stringify(votos);
      });
  <?php endif ?>
  }
  function minus(id){
  <?php if(empty($_SESSION['user'])): ?>
      $("#iniciar-sesion").modal('toggle');
  <?php else: ?>
      remoteConnection('listas/frontend/minus/'+id,null,function(data){
      $(".votar"+id).find('.numero-menos').removeClass('numero-menos');
      $(".votar"+id).find('.numero-mas').removeClass('numero-mas');
      $(".votar"+id).find('a').addClass('numero-bloqueado');
      $(".votar"+id).find('a').attr('href','#');
      var votos = JSON.parse(localStorage.votos);
      votos.push(id);
      localStorage.votos = JSON.stringify(votos);
      });
  <?php endif ?>
  }

  function ordenarIndex(){
    var x = 0;
    $("#reordenar .contenedor-tarjeta-redeem").each(function(){
        $(this).find('input').each(function(){
            var name = $(this).data('name');
            $(this).attr('name','detalles['+x+']['+name+']');
        });
        $(this).find('.indice').html(x+1);
        console.log(x);
        x++;
    })
  }

  function guardarLista(form){

  ordenarIndex();
  <?php if(empty($_SESSION['user'])): ?>
      $("#iniciar-sesion").modal('toggle');
  <?php else: ?>


    <?php if(empty($_SESSION['user']) || $this->user->id!=$lista->user_id): ?>
    var f = new FormData(form);
    insertarUser('listas/frontend/reordenarListaUserCrud/update/<?= $lista->id ?>',f,function(data){
        if(data.success){
            $("#result").addClass('alert alert-success').html('Se ha almacenado su lista con éxito');
        }
    });
    <?php else: ?>
      alert("Detección de autoreordenado")
    <?php endif ?>

  <?php endif ?>


    return false;
}
  </script>


<script src="//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v3.1&appId=518978775229184&autoLogAppEvents=1"></script>
<script>


  localStorage.facebookShare = typeof(localStorage.facebookShare)==='undefined'?'[]':localStorage.facebookShare;
  //Share Facebook
  function shareFacebook(id,title,link,picture) {
    <?php if(empty($_SESSION['user'])): ?>
      $("#iniciar-sesion").modal('toggle');
    <?php else: ?>
      var shares = JSON.parse(localStorage.facebookShare);
      if(shares.indexOf(id)>-1){
        alert("Ya has compartido esta lista");
      }else{
        FB.init({
          appId: '518978775229184', //replace with your app ID
          version: 'v3.1'
        });
        FB.ui({
            method: 'share',
            title: title,
            href: link,
          },
          function(response) {
            if (response && !response.error_code) {
               $.get('<?= base_url('listas/frontend/share/') ?>/'+id+'/2',{},function(){
                  var shares = JSON.parse(localStorage.facebookShare);
                  shares.push(id);
                  localStorage.facebookShare = JSON.stringify(shares);
               });
            }
        });
      }
      <?php endif ?>
  }

  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      console.log(response);
    });
  }

</script>



<script src="https://platform.twitter.com/widgets.js"></script>

<script>
// Wait until twttr to be ready before adding event listeners
twttr.ready(function (twttr) {
  twttr.events.bind('tweet', function(event) {
    var id = $(event.target).data('id');
    $.get('<?= base_url('listas/frontend/share/') ?>/'+id+'/1',{},function(){});
  });
});

function iniciarLogin(){
   $("#iniciar-sesion").modal('toggle');
}
</script>


<script>
  var rowNumber = $("#reordenar .contenedor-tarjeta-redeem").length+1;
  var row = $("#reordenar .participantRow");
  function addRow() {
    if($(".participantRow").css('display')=='none' && $("#reordenar .contenedor-tarjeta-redeem").length<=10){
      $(".participantRow").show();
       ordenarIndex();
    }else{
      if($("#reordenar .contenedor-tarjeta-redeem").length<10){
        console.log(row);
        var newrow = row.clone(true, true);
        var totalInputs = newrow.find('input').length;
        newrow.find('.indice').html(rowNumber);
        newrow.find('input').val('');
        newrow.find('img').attr('src','<?= base_url('theme/theme/assets/img/Perfiles/perfil.jpg') ?>');
        var x = 0;
        newrow.find('input').each(function(){
            x++;
            $(this).attr('name','detalles['+rowNumber+']['+$(this).data('name')+']');
            if(x===totalInputs){
              newrow.appendTo("#left-rollbacks");
              rowNumber++;
              ordenarIndex();
            }
        });
      }
    }

  }

  function removeRow(button) {
   button.parents(".participantRow").html($("#emptyRows").html());
   ordenarIndex();
  }
  /* Doc ready */
  $(".add").on('click', function () {


  if($("#participantTable .participantRow").length < 10) {
    addRow();
    /*var i = Number(p)+1;
    $("#participants").val(i);*/
  }
  $(this).closest("tr").appendTo("#participantTable");
  if ($("#participantTable .participantRow").length === 3) {
      $(".remove").hide();
  } else {
      $(".remove").show();
  }
  });
  $(".remove").on('click', function () {
      if($("#participantTable .participantRow").length === 3) {
        //alert("Can't remove row.");
        $(".remove").hide();
      } else if($("#participantTable .participantRow").length - 1 ==3) {
        $(".remove").hide();
        removeRow($(this));
        var i = Number($("#participantTable .participantRow").length)+1;
        $("#participants").val(i);
      } else {
        removeRow($(this));
        var i = Number($("#participantTable .participantRow").length)+1;
        $("#participants").val(i);
      }
  });

  $(".remove2").on('click',function(){
    $(this).parents('.contenedor-tarjeta-redeem').replaceWith($("#emptyRows").html());
    ordenarIndex();
  });
</script>