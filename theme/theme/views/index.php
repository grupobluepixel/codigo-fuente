    <!-- Slider TOP -->
    <div class="container-fluid contenedor-slider-top fadeOut owl-carousel owl-theme">
        <?php include('Carrousel/slider-top.php');?>
    </div>

    <!-- Snacktrend -->
    <div class="container-fluid fondo-gradient-azul">
        <?php include('snacktrend.php');?>
    </div>

     <!-- Slider Categorias -->
    <div class="container-fluid contenedor-slider-categorias">
          <?php include('Carrousel/slider-categorias.php');?>
    </div>

    <?php
        $this->db->order_by('orden','ASC');
        foreach($this->db->get_where('categorias_home')->result() as $c):
    ?>
        <?php $this->load->view('views/Carrousel/categorias_home',array('c'=>$c)); ?>
    <?php endforeach ?>

    <div class="container-fluid fondo-blanco">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 contenedor-ads">Google ADS</div>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php $this->load->view('views/footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php $this->load->view('views/footer-mapa.php');?>
    </footer>

    <!-- Modales -->
    <?php $this->load->view('views/modales.php');?>

    <!-- Librerias -->
    <?php $this->load->view('views/librerias.php');?>
    <script>
         $(document).ready(function() {
            materialKit.initSliders();

        });
    </script>
