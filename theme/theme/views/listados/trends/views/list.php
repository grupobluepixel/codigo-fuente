<table class="table">
	<thead>		
		<tr>    
	    	<?php foreach ($columns as $column): ?>
	    
	            <th><?php echo $column->display_as ?>  </th>
	    
	    	<?php endforeach ?>
		</tr>
	</thead>

	<tbody>
		<?php foreach ($list as $num_row => $row): ?>      			
			<tr>    
		    	<?php foreach ($columns as $column): ?>
		            <td><?php echo $row->{$column->field_name} != '' ? $row->{$column->field_name} : '&nbsp;'; ?></td>
		    	<?php endforeach ?>
    		</tr>			
		<?php endforeach ?>	
	</tbody>
</table>