<?php foreach ($list as $num_row =>$row): ?>
   <div class="col-xs-12 col-sm-6 col-md-3">

      <!--
      <div class="media header-categoria">
          <div class="imagen-categorias center-block">
              <img src="<?= base_url('img/categorias/'.$row->icono) ?>" alt="Categoria Snacktrend" class="img-responsive img-circle">
          </div>
          <div class="media-body"><b><?= $row->nombre ?> (<?= $row->total ?>)</b></div>
      </div>-->

      <div class="header-categoria" style="display: flex;">
          <div><img src="<?= base_url('img/categorias/'.$row->icono) ?>" alt="Categoria Snacktrend" class="img-responsive img-circle"></div>
          <div><b><?= $row->nombre ?> (<?= $row->total ?>)</b></div>
      </div>

      <!--
      <table class="table table-striped">
        <tbody>
            <?php foreach($row->subcategorias->result() as $s): ?>
              <tr>
                  <td><a href="<?= base_url('subcategoria/'.toUrl($s->id.'-'.$s->nombre)) ?>"><?php get_instance()->db->group_by('listas_id'); echo get_instance()->db->get_where('listas_categorias',array('subcategorias_id'=>$s->id))->num_rows(); ?></a></td>
                  <td><a href="<?= base_url('categoria/'.toUrl($s->id.'-'.$s->nombre)) ?>"><?= $s->nombre ?></a></td>
              </tr>
            <?php endforeach ?>
        </tbody>
      </table>-->


      <div class="tabla-categorias">
          <?php foreach($row->subcategorias->result() as $s): ?>
              <div class="contenedor-tabla-categoria">
                <b>
                <a href="<?= base_url('subcategoria/'.toUrl($s->id.'-'.$s->nombre)) ?>"><?php get_instance()->db->group_by('listas_id'); echo get_instance()->db->get_where('listas_categorias',array('subcategorias_id'=>$s->id))->num_rows(); ?></a>
              </b>

                <a href="<?= base_url('subcategoria/'.toUrl($s->id.'-'.$s->nombre)) ?>"><?= $s->nombre ?></a>
              </div>
          <?php endforeach ?>
      </div>

  </div>
<?php endforeach ?>
<?php if(count($list)==0): ?>

   <div class="col-xs-12 col-sm-12 titulo-categorias text-center">
        <big><b>En estos momentos no existen subcategorias para esta categoria</b></big>
   </div>

<?php endif ?>
