<?php foreach ($list as $num_row =>$row): ?>
<!-- Notificacion -->
  <div class="row text-center">
      <div class="col-md-12 contenedor-notificacion sin-borde-notificacion">
          <div class="imagen-notificacion">
              <img src="<?= empty($row->avatar)?get_instance()->querys->get_perfil(get_instance()->user->foto):get_instance()->querys->get_avatar($row->avatar) ?>" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
          </div>
          <div class="texto-notificacion">
            <?= $row->comentario ?>
            <small class="texto-azul-light"><?= $row->fecha ?></small>
          </div>
      </div>
  </div>
  <!-- Notificacion -->
<?php endforeach ?>
