




<?php foreach ($list as $num_row =>$row): ?>
   <div class="col-sm-3">
      <a href="<?= base_url('subcategoria/'.toUrl($row->id.'-'.$row->nombre)) ?>">
        <div class="table-responsive tabla-categorias">
                <table class="table table-striped">

                      <thead class="titulo-categorias">
                          <div class="media header-categoria">
                              <div class="imagen-categorias center-block">
                                  <img src="<?= base_url('img/categorias/'.$row->icono) ?>" alt="Perfil Snacktrend" class="img-responsive img-circle">
                              </div>
                              <div class="media-body"><b><?= $row->nombre ?> (<?= $row->listas ?>)</b></div>
                          </div>
                      </thead>

                </table>
          </div>
        </a>
  </div>
<?php endforeach ?>
<?php if(count($list)==0): ?>
 <div class="col-sm-12 text center titulo-categorias">
      <big><b>En estos momentos no existen subcategorias para esta categoria</b></big>
 </div>
<?php endif ?>
