<?php foreach ($list as $num_row =>$row): ?>


<!--
   <div class="col-sm-3">
      <div class="contenedor-tarjeta-blanco fondo-blanco">
            <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> <?= $row->lista->row()->vistas ?></div>

            <a href="<?= $row->lista->row()->link ?>">
                <div class="header-tarjeta-categoria texto-gris-oscuro">
                    <h3><?= $row->lista->row()->titulo_corto ?></h3>
                </div>
                <div class="footer-tarjeta-snacktrend-top6">
                    <?php foreach($row->lista->row()->detalles_sort as $d): ?>
                      <div class="contenedor-listado-categoria">
                          <div class="imagen-listado-tarjeta"  style="background:url(<?= $d->adjunto ?>)">
                              <div class="top-listado-tarjeta texto-blanco"><?= $d->posicion ?></div>
                          </div>
                          <div class="listado-tarjeta"><b><?= $d->nombre ?></b></div>
                      </div>
                    <?php endforeach ?>                    
                </div>
            </a>

            <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                <div class="perfil-tarjeta-categorias"><img src="<?= $row->lista->row()->perfil ?>" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                <div class="username-tarjetas"><b><?= $row->lista->row()->username ?></b><br>Votos:<?= $row->lista->row()->votos ?></div>
            </div>
      </div>
  </div>-->





<div class="col-sm-3">
  <div class="contenedor-tarjeta-blanco fondo-blanco">

          <div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
              <div class="col-xs-6 col-sm-6" title="Vistas de la Lista" style="padding-right:0px; padding-left:0px;">
                  <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> <?= $row->lista->row()->vistas ?></div>
              </div>
              <div class="col-xs-6 col-sm-6" title="Fecha de creación de la Lista" style="padding-right:0px; padding-left:0px;">
                  <div class="fecha-tarjetas icon-vistas">29/09/2018</div>
              </div>
          </div>

          <div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
            <!--
            <span class="categoria-snacktrend">
              <a href="http://labtico.com/categoria/1-mascotas-y-animales">Mascotas y animales</a>/
              <a href="http://labtico.com/subcategoria/1-perros">Perros</a>
            </span>-->

            <a href="<?= $row->lista->row()->link ?>" class="link-tarjetas">
              <div class="header-tarjeta-categoria"><h3><?= $row->lista->row()->titulo_corto ?></h3></div>

              <div class="height-listas-principales">

                  <?php foreach(get_instance()->querys->get_sort($row->lista->row()->id) as $detall):
                    $detall->foto = get_instance()->querys->get_foto($detall->adjunto);
                  ?>
                <div class="col-xs-12 col-sm-12" style="display:flex; margin-bottom:10px;">
                    <div class="imagen-listado-tarjeta" style="background:url('<?= get_instance()->querys->get_foto($detall->adjunto) ?>'); background-size: cover; background-position:center center;">
                      <div class="top-listado-tarjeta texto-blanco"><?= $detall->posicion ?></div>
                    </div>
                    <div class="listado-tarjeta"><b><?= $detall->nombre ?></b><br><?= $detall->autor ?></div>
                </div>
                <?php endforeach ?>
                  

              </div>
            </a>
          </div>

          <div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
              <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                  <div class="perfil-tarjeta-categorias"><img src="<?= $row->lista->row()->perfil ?>" alt="Perfil Snacktrend" class="center-block img-responsive img-circle" style="width:30px; height:30px;"></div>
                  <div class="username-tarjetas"><b><?= $row->lista->row()->username ?></b><br>Votos:<?= $row->lista->row()->votos ?></div>
              </div>
          </div>

        </div>
</div>


<?php endforeach ?>
<?php if(count($list)==0): ?>
   <div class="col-sm-12 text-center">
        <big><b>En estos momentos no existen subcategorias para esta categoria</big></b>
    </div>

    <div class="col-sm-12 contenedor-tabs-redeem center-block">
        <a href="http://www.labtico.com/categorias">
            <button style="background-color:transparent; border:0px;"><div class="text-center btn-menu-top btn-general btn-derecha"><b>Regresar al listado</b></div></button>
        </a>
    </div>

<?php endif ?>