

<?php
$this->set_css($this->default_theme_path.'perfil/css/flexigrid.css');
$this->set_js_lib($this->default_theme_path.'perfil/js/jquery.form.js');
$this->set_js_config($this->default_theme_path.'perfil/js/flexigrid-edit.js');
$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
?>


<div data-unique-hash="<?php echo $unique_hash; ?>">
  <?php echo form_open( $update_url, 'method="post" id="crudForm" autocomplete="off" enctype="multipart/form-data"'); ?>

  <div class="row text-center superior-perfil">
    <div class="col-xs-12 col-md-12 text-center"><h2><b>Editar perfil</b></h2></div>


    <!-- Icono de cámara si aun no suben una foto, lo copie del modal de registro
    <div class="col-sm-12">
        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
            <span class="btn btn-file">
                <span class="fileinput-new">
                    <div class="center-block" id="img-login-registro"><i class="material-icons">camera_alt</i></div>
                </span>
                <div class="col-sm-12"><img id="imagePreviewRegistro" alt="" src="" style="display: none; width:15%;"></div>
                <input name="s66c9eed1" id="fotoFile" class="registrofoto" type="file">
            </span>
            <a href="#" id="borrarFotoRegistro" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
        </div>
    </div>-->

    <div class="col-xs-12 col-md-12 btn-image-perfil">
      <div class="fileinput fileinput-new text-center editar-foto-perfil" data-provides="fileinput">
        <?php echo $input_fields['foto']->input ?>        
      </div>
    </div>
  </div>

  <div class="row contenedor-datos-editar">
    <div class="col-xs-12 col-md-12">


     <div class="col-xs-12 col-sm-12 col-md-12">
       <div class="contenedor-seccion-informativa">
         <div><b>Datos de tu Perfil</b></div>
         <?php
                    foreach($fields as $field): if($field->field_name!='foto' && $field->field_name!='genero'):
                ?>
           <div class="form-group">
             <label for="exampleInputEmail1"><?php echo $input_fields[$field->field_name]->display_as; ?></label>
             <?php echo $input_fields[$field->field_name]->input ?>
           </div>

         <?php endif; endforeach ?>
         <!--<div class="genero-login">
            <div class="form-check margin-bottom-modal genero-modal-registro">
                <label class="form-check-label">
                    <input class="form-check-input" name="genero" id="field-genero-option1" value="Masculino" <?= $input_fields['genero']->input=='Masculino'?'checked':'' ?> type="radio">
                    Hombre
                    <span class="circle"><span class="check"></span></span>
                </label>
            </div>
            <div class="form-check margin-bottom-modal genero-modal-registro">
                <label class="form-check-label">
                    <input class="form-check-input" name="genero" id="field-genero-option2" value="Femenino" <?= $input_fields['genero']->input=='Femenino'?'checked':'' ?> type="radio">
                    Mujer
                    <span class="circle"><span class="check"></span></span>
                </label>
            </div>
        </div>-->

         <div id='report-error' style="display:none" class='alert alert-danger'></div>
         <div id='report-success' style="display:none" class='alert alert-success'></div>
         <div class="col-xs-12 col-sm-12 col-md-12 text-center" style="padding-left:0px; padding-right:0px;">
           <button id="form-button-save" type='submit' class="btn-menu-top btn-general btn-derecha pull-right center-block">
             <?php echo $this->l('form_edit'); ?>
           </button>
         </div>

       </div>
     </div>

   </div>
   <?php echo form_close(); ?>
  </div>

</div>


 <script>
var validation_url = '<?php echo $validation_url?>';
var list_url = '<?php echo $list_url?>';
var message_alert_edit_form = "<?php echo $this->l('alert_edit_form')?>";
var message_update_error = "<?php echo $this->l('update_error')?>";
$("input[type='text'],input[type='password']").addClass('form-control');
</script>




