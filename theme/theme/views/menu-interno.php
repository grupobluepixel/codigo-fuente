  <li class="dropdown nav-item" id="campana">
      <?php $mensajes = $this->querys->get_mensajes($this->user->id); ?>
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <img src="<?= base_url() ?>theme/theme/assets/img/Iconos/notificaciones.png" class="img-notificaciones">
        <span class="badge badge-light"><?= $mensajes->num_rows()==0?'':$mensajes->num_rows(); ?></span>
      </a>

      <div class="dropdown-menu" id="dropdown-notificaciones">
          <div class="row fondo-iniciar-sesion02 text-center">
            <?php foreach($mensajes->result() as $n=>$m): if($n<5): ?>
              <a href="<?= empty($m->url)?base_url('mensajeria'):base_url($m->url); ?>">
                  <div class="col-md-12 contenedor-notificacion-dropdown borde-notificacion">
                      <div class="imagen-notificacion-dropdown">
                          <img src="<?= empty($m->avatar)?$this->querys->get_perfil($this->user->foto):$this->querys->get_avatar($m->avatar) ?>" alt="Perfil Snacktrend" class="img-circle">
                      </div>
                      <div class="texto-notificacion-dropdown">
                        <?= $m->comentario ?>
                        <small class="texto-azul-light"><?= date("d/m/Y",strtotime($m->fecha)) ?></small>
                      </div>
                  </div>
              </a>
            <?php endif; endforeach ?>
            <?php if($mensajes->num_rows()==0): ?>
              <div class="col-md-12 contenedor-notificacion-dropdown">
                  <div class="texto-notificacion-dropdown">
                    No hay mensajes nuevos en este momento
                  </div>
              </div>
            <?php endif ?>

              <a href="<?= base_url('mensajeria') ?>"><div class="col-sm-12 btn-ver-notificaciones" style="width:100%;">Ver todas las notificaciones</div></a>
          </div>
      </div>

  </li>

  <li class="dropdown nav-item">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <img src="<?= $this->querys->get_perfil($this->user->foto) ?>" class="img-notificaciones" style="max-width:50px; max-height:50px;">
        <span class="badge badge-light" style="background-color: #3FA0E8;"><?= $this->querys->get_puntos($this->user->id)->total ?></span>
      </a>

      <div class="dropdown-menu" id="dropdown-menu02">
          <div class="row fondo-iniciar-sesion02 text-center">
              <div class="col-sm-12 texto-aviso-login02">
                <a href="<?= base_url('mis-listas') ?>"><i class="fa fa fa-bars"></i> Mis listas</a>
              </div>
              <div class="col-sm-12 texto-aviso-login02">
                  <a href="<?= base_url() ?>snacks"><i class="fa fa-gift"></i> Mis snacks</a>
              </div>
              <div class="col-sm-12 texto-aviso-login02">
                  <a href="<?= base_url() ?>perfil"><i class="fa fa-user"></i> Editar Perfil</a>
              </div>
              <div class="col-sm-12 texto-aviso-cerrar">
                <a href="<?= base_url('main/unlog') ?>"> <i class="fa fa-sign-out"></i> Cerrar sesión</a>
              </div>
          </div>
      </div>
  </li>
