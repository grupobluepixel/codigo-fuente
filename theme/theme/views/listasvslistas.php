
    <div class="container-fluid contenedor-ads margin-secciones-menu-top">
        <div class="container">
            <div class="row text-center">Google ADS</div>
        </div>
    </div>

    <?php
        $metricas = $this->querys->get_metricas($this->user->id);
        $puntajes = $this->querys->get_puntos($this->user->id);
        $nivel = (object)$this->querys->get_nivel($this->user->id);
        $porcentaje = $nivel->siguienteNivel!=0?($puntajes->total*100)/$nivel->siguienteNivel:100;
        $porcentaje = ceil($porcentaje);
        $votos_negativos = $metricas->total_votos>0?round(($metricas->votos_negativos*100)/$metricas->total_votos,0):0;
        $votos_positivos = $metricas->total_votos>0?round(($metricas->votos_positivos*100)/$metricas->total_votos,0):0;

    ?>

    <div class="container-fluid fondo-gradient-azul contenedor-preguntas">
        <div class="container fondo-preguntas padding0">
            <div class="row titulo-seccion">
                <div class="col-md-12 text-center"><h2><b>Mis Snacks</b></h2></div>
            </div>

            <!--
            <div class="row" style="background-color: #333333; color:white;">
                <nav class="col-sm-12 navbar navbar-expand-lg navbar-light" id="nav-listas">
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                          <li class="nav-item">
                            <a class="nav-link" href="#menu-categorias"><b>Ver mis listas de Animales y mascotas</b></a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="#">Categoría 2</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="#">Categoría 3</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="#">Categoría 4</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="#">Categoría 5</a>
                          </li>
                        </ul>
                    </div>
                </nav>
            </div>-->

            <div class="contendor-fondo-notificaciones">
                <div class="row">
                        <div class="col-sm-12">
                                <div class="col-sm-4">
                                        <div class="">
                                            <div class="col-md-12 snackteam">

                                                <div class="col-md-12 perfil-mislistas text-center">
                                                    <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/sesion.png" alt="Perfil Snackteam" class="img-responsive center-block">
                                                    <b class="texto-gris-oscuro">Nombre: </b>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="col-sm-12 margin-barra">
                                                        <div class="col-sm-12 text-center"><b>Nivel: <?= $nivel->nivel ?></b></div>
                                                        <div class="col-sm-12">
                                                            <div class="progress-listas">
                                                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="<?= $porcentaje ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $porcentaje ?>%"></div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!--
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-12">
                                                            <div class="col-sm-6"><b>Calificación del Autor</b></div>
                                                            <div class="col-sm-6 nivel-izquierda">
                                                                <i class="material-icons">thumb_up</i> <b><?= $votos_positivos ?>%</b>
                                                                <i class="material-icons">thumb_down</i> <b><?= $votos_negativos ?>%</b>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="progress">
                                                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="<?= $votos_positivos ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $votos_positivos ?>%"></div>
                                                            </div>
                                                        </div>
                                                    </div>-->
                                                </div>
                                                <div class="col-md-12 texto-listasvslistas">
                                                  Suma puntos al publicar, votar y reordenar las listas. Compártelas en tus redes sociales. ¡Sé un influencer!<br>
                                                  <b><big>5,000 Snacks</big></b>
                                                </div>
                                                <div class="col-md-12" style="text-align: center">
                                                      <img src="<?= $nivel->logo ?>" class="img-responsive center-block">
                                                      <div class="nivel-editar">Nivel <?= $nivel->nivel ?></div>
                                                      <div class="titulo-editar"><b><?= $nivel->label ?></b></div>
                                                      <span class="btn-niveles" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#niveles-usuario">
                                                        <i class="material-icons">info</i>
                                                      </span>
                                                </div>
                                            </div>
                                        </div>
                                </div>

                                <div class="col-sm-8">
                                        <div class="col-sm-12 text-center">
                                            <div class="col-xs-12 col-md-12 texto-azul-light center-block">
                                                <!-- Estadisticas -->
                                                <div class="col-xs-6 col-md-3">
                                                    <div class="fondo-blanco contenedor-cifras">
                                                        <div class="numero-01">
                                                            <big><?= $metricas->listas  ?></big><br>
                                                            <small>Has publicado</small>
                                                        </div>
                                                        <div class="numero-02">
                                                            <big><?= $puntajes->listas ?></big><br>
                                                            <small>Snacks</small>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Estadisticas -->
                                                <div class="col-xs-6 col-md-3">
                                                    <div class="fondo-blanco contenedor-cifras">
                                                        <div class="numero-01">
                                                            <big><?= $metricas->has_votado ?></big><br>
                                                            <small>Has votado</small>
                                                        </div>
                                                        <div class="numero-02">
                                                            <big><?= $puntajes->has_votado ?></big><br>
                                                            <small>Snacks</small>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Estadisticas -->
                                                <div class="col-xs-6 col-md-3">
                                                    <div class="fondo-blanco contenedor-cifras">
                                                        <div class="numero-01">
                                                            <big><?= $metricas->has_reordenado ?></big><br>
                                                            <small>Has reordenado</small>
                                                        </div>
                                                        <div class="numero-02">
                                                            <big><?= $puntajes->has_reordenado ?></big><br>
                                                            <small>Snacks</small>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Estadisticas -->
                                                <div class="col-xs-6 col-md-3">
                                                    <div class="fondo-blanco contenedor-cifras">
                                                        <div class="numero-01">
                                                            <big><?= $metricas->has_compartido ?></big><br>
                                                            <small>Has compartido</small>
                                                        </div>
                                                        <div class="numero-02">
                                                            <big><?= $puntajes->has_compartido ?></big><br>
                                                            <small>Snacks</small>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Estadisticas -->
                                                <div class="col-xs-6 col-md-3">
                                                    <div class="fondo-blanco contenedor-cifras">
                                                        <div class="numero-01">
                                                            <big><?= $metricas->votos_terceros ?></big><br>
                                                            <small>Han votado en tus listas</small>
                                                        </div>
                                                        <div class="numero-02">
                                                            <big><?= $puntajes->votos_terceros ?></big><br>
                                                            <small>Snacks</small>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Estadisticas -->
                                                <div class="col-xs-6 col-md-3">
                                                    <div class="fondo-blanco contenedor-cifras">
                                                        <div class="numero-01">
                                                            <big><?= $metricas->ediciones_terceros ?></big><br>
                                                            <small>Han reordenado tus listas</small>
                                                        </div>
                                                        <div class="numero-02 fondo-gris">
                                                            <big><?= $puntajes->ediciones_terceros ?></big><br>
                                                            <small>Snacks</small>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Estadisticas -->
                                                <div class="col-xs-6 col-md-3">
                                                    <div class="fondo-blanco contenedor-cifras">
                                                        <div class="numero-01">
                                                            <big><?= $metricas->te_han_compartido ?></big><br>
                                                            <small>Han compartido tus listas</small>
                                                            <!--
                                                            <div class="redes-redeem">
                                                                <a href="#"><i class="fa fa-facebook-official"></i>: 555</a> |
                                                                <a href="#"><i class="fa fa-twitter"></i>: 100</a>
                                                            </div>-->
                                                        </div>
                                                        <div class="numero-02">
                                                            <big><?= $puntajes->te_han_compartido ?></big><br>
                                                            <small>Snacks</small>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Estadisticas -->
                                                <div class="col-xs-6 col-md-3">
                                                    <div class="fondo-blanco contenedor-cifras">
                                                        <div class="numero-01">
                                                            <big><?= $metricas->trends ?></big><br>
                                                            <small>Listas en snacktrends</small>
                                                            <!--
                                                            <div class="redes-redeem">
                                                                <a href="#"><i class="fa fa-facebook-official"></i>: 555</a> |
                                                                <a href="#"><i class="fa fa-twitter"></i>: 100</a>
                                                            </div>-->
                                                        </div>
                                                        <div class="numero-02">
                                                            <big><?= $puntajes->trends ?></big><br>
                                                            <small>Snacks</small>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Estadisticas -->
                                                <div class="col-xs-6 col-md-3">
                                                    <div class="fondo-blanco contenedor-cifras">
                                                        <div class="numero-01">
                                                            <big>1</big><br>
                                                            <small>Snacks iniciales</small>
                                                            <!--
                                                            <div class="redes-redeem">
                                                                <a href="#"><i class="fa fa-facebook-official"></i>: 555</a> |
                                                                <a href="#"><i class="fa fa-twitter"></i>: 100</a>
                                                            </div>-->
                                                        </div>
                                                        <div class="numero-02">
                                                            <big>50</big><br>
                                                            <small>Snacks</small>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-sm-12 text-center">
                                            <div class="col-md-12 texto-azul-light center-block puntos-totales">
                                                <div class="numero-total"><big><b>Total: <?= $puntajes->total ?></b></big></div>
                                            </div>
                                        </div>
                                </div>
                        </div>
                </div>
            </div>


        </div>
    </div>

    <?php if(!empty($lista)): ?>


    <!--- Comparación 
    <div class="container-fluid contenedor-snackteam fondo-blanco" id="menu-categorias">

        <div class="container">

            <?php if($lista->categorias->num_rows()>0): ?>
                <div class="row margin-titulo-slider-home">
                    <h2 class="titulo-gastronomia texto-blanco">
                      <a href="<?= base_url('categoria/'.toUrl($lista->categorias->row()->id.'-'.$lista->categorias->row()->nombre)) ?>">
            			    	   <img src="<?= base_url('img/categorias/'.$lista->categorias->row()->icono) ?>">
            						<?= $lista->categorias->row()->nombre ?>
                      </a>
                    </h2>
                </div>
            <?php endif ?>

            <div class="row">
                <div class="col-xs-12 col-sm-12">



                    <div class="col-xs-12 col-sm-6 lista-original">

                        <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> <?= $original->vistas ?></div>
                        <div class="header-tarjeta-categoria texto-gris-oscuro">
                            <b>Mi lista original</b><br>
                            <?php if($lista->categorias->num_rows()>0): ?>
                                <a href="<?= base_url('categoria/'.toUrl($lista->categorias->row()->id.'-'.$lista->categorias->row()->nombre)); ?>">
                                    <span class="categoria-snacktrend"><?= @$original->categorias->row()->nombre ?></span>
                                </a>
                            <?php endif ?>
                        </div>
                        <a href="<?= base_url('lista/'.toUrl($lista->id.'-'.$lista->titulo_corto)) ?>">
                            <div class="texto-gris-oscuro">
                                <h3><b><?= $original->titulo ?></b></h3>
                            </div>
                            <?php foreach($original->detalles->result() as $d): ?>
                                <div class="contenedor-listado-categoria">
                                    <div class="imagen-listado-tarjeta" style="background:url('<?= get_instance()->querys->get_foto($d->adjunto) ?>'); background-size:cover; background-position: center center;">
                                        <div class="top-listado-tarjeta texto-blanco"><?= $d->posicion ?></div>
                                    </div>
                                    <div class="listado-tarjeta"><b><?= $d->nombre ?></b><br><?= $d->autor ?></div>
                                </div>
                            <?php endforeach ?>
                        </a>
                        <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                            <div class="perfil-tarjeta-categorias">
                                <img src="<?= $this->querys->get_perfil($original->perfil) ?>" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
                            </div>
                            <div class="username-tarjetas"><b>Username <?= $original->username ?></b><br>Votos:<?= $original->votos ?></div>
                        </div>

                    </div>


                    <div class="col-xs-12 col-sm-6">
                        <div class="margin-listas-comparacion">
                            <div class="vistas-tarjetas icon-vistas">
                                  <i class="fa fa-eye" aria-hidden="true"></i> <?= $lista->votos ?>
                            </div>
                            <div class="header-tarjeta-categoria texto-gris-oscuro">
                                <b>Lista Actualizada</b><br>
                                <?php if($lista->categorias->num_rows()>0): ?>
                                    <a href="<?= base_url('categoria/'.toUrl($lista->categorias->row()->id.'-'.$lista->categorias->row()->nombre)); ?>">
                                      <span class="categoria-snacktrend"><?= @$lista->categorias->row()->nombre ?></span>
                                    </a>
                                <?php endif ?>
                            </div>
                            <a href="<?= base_url('lista/'.toUrl($lista->id.'-'.$lista->titulo_corto)) ?>">
                                <div class="texto-gris-oscuro">
                                    <h3><b><?= $lista->titulo ?></b></h3>
                                </div>
                                <div class="">
                                    <?php foreach($lista->detall->result() as $p): ?>
                                    <div class="contenedor-listado-categoria ">
                                        <div class="imagen-listado-tarjeta" style="background:url('<?= get_instance()->querys->get_foto($p->adjunto) ?>'); background-size:cover; background-position: center center;">
                                          <div class="top-listado-tarjeta texto-blanco"><?= $p->posicion ?></div>
                                        </div>
                                        <div class="listado-tarjeta"><b><?= $p->nombre ?></b><br><?= $p->autor ?></div>
                                    </div>
                                    <?php endforeach ?>

                                </div>
                            </a>
                        </div>
                    </div>

                </div> ----->
            </div>

        </div>
    </div>
    <?php endif ?>

    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php include('footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php include('footer-mapa.php');?>
    </footer>

    <!-- Modales -->
    <?php include('modales.php');?>

    <!-- Librerias -->
    <?php include('librerias.php');?>

    <!-- Anclas Menu -->
    <script>
      /*$('nav a').click(function(e){
  		e.preventDefault();		//evitar el eventos del enlace normal
  		var strAncla=$(this).attr('href'); //id del ancla
  			$('body,html').stop(true,true).animate({
  				scrollTop: $(strAncla).offset().top
  			},1000);

  	});*/
    </script>
