<div class="contenedor-snackteam fondo-blanco" id="menu-categorias">
	<div class="row">
		<div class="col-xs-12 col-sm-12">
			<!--- Original ---->
			<div class="col-xs-12 col-sm-6">
				<div class="vistas-tarjetas icon-vistas">
					<i class="fa fa-eye" aria-hidden="true"></i> <?= $original->vistas ?>
				</div>
				<div class="header-tarjeta-categoria texto-gris-oscuro">
					<b>Mi lista original</b><br>
					<a href="<?= base_url('categoria/'.toUrl($lista->categorias->row()->id.'-'.$lista->categorias->row()->nombre)); ?>">
						<span class="categoria-snacktrend"><?= @$original->categorias->row()->nombre ?></span>
					</a>
				</div>
				<a href="<?= base_url('lista/'.toUrl($lista->id.'-'.$lista->titulo_corto)) ?>">
					<div class="texto-gris-oscuro">
						<h3><b><?= $original->titulo ?></b></h3>
					</div>
					<?php foreach($original->detalles->result() as $d): ?>
						<div class="contenedor-listado-categoria">
							<div class="imagen-listado-tarjeta" style="background:url('<?= get_instance()->querys->get_foto($d->adjunto) ?>'); background-size:cover; background-position: center center;">
								<div class="top-listado-tarjeta texto-blanco"><?= $d->posicion ?></div>
							</div>
							<div class="listado-tarjeta"><b><?= $d->nombre ?></b><br><?= $d->autor ?></div>
						</div>
					<?php endforeach ?>
				</a>
			</div>
			<!--Final ------->
			<div class="col-xs-12 col-sm-6">
				<div class="margin-listas-comparacion">
					<div class="vistas-tarjetas icon-vistas">
						<i class="fa fa-eye" aria-hidden="true"></i> <?= $lista->vistas ?>
					</div>
					<div class="header-tarjeta-categoria texto-gris-oscuro">
						<b>Lista Actualizada</b><br>
						<a href="<?= base_url('categoria/'.toUrl($lista->categorias->row()->id.'-'.$lista->categorias->row()->nombre)); ?>">
							<span class="categoria-snacktrend"><?= @$lista->categorias->row()->nombre ?></span>
						</a>
					</div>
					<a href="<?= base_url('lista/'.toUrl($lista->id.'-'.$lista->titulo_corto)) ?>">
						<div class="texto-gris-oscuro">
							<h3><b><?= $lista->titulo ?></b></h3>
						</div>
						<div class="">
							<?php foreach($lista->detalles->result() as $d): ?>
								<div class="contenedor-listado-categoria">
									<div class="imagen-listado-tarjeta" style="background:url('<?= get_instance()->querys->get_foto($d->adjunto) ?>'); background-size:cover; background-position: center center;">
										<div class="top-listado-tarjeta texto-blanco"><?= $d->posicion ?></div>
									</div>
									<div class="listado-tarjeta"><b><?= $d->nombre ?></b><br><?= $d->autor ?></div>
								</div>
							<?php endforeach ?>
							
						</div>
					</a>
				</div>
			</div>
			<!--Fin comparación ---->
		</div>
	</div>
</div>