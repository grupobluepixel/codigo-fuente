

<script>
  jQuery(document).ready(function($) {
    $('.fadeOut').owlCarousel({
      items: 1,
      animateOut: 'fadeOut',
      loop: true,
      margin: 10,
      autoplay: true,
      autoplayTimeout: 4000,
      autoplayHoverPause: true
    });
    $('.custom1').owlCarousel({
      animateOut: 'slideOutDown',
      animateIn: 'flipInX',
      items: 1,
      margin: 30,
      stagePadding: 100,
      smartSpeed: 450
    });
  });
</script>

<!-- Drag & Drop -->
<script src='<?= base_url() ?>theme/theme/Drag-drop/dragula.js'></script>
<script src='<?= base_url() ?>theme/theme/Drag-drop/example.min.js'></script>

<!-- Menu -->
<script>

if(document.getElementById("myHeader")){
    window.onscroll = function() {myFunction()};
    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;
    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else if(header) {
        header.classList.remove("sticky");
      }
    }
}
</script>

<!--   Core JS Files   -->
<script src="<?= base_url() ?>theme/theme/assets/js/core/jquery.min.js"></script>
<script src="<?= base_url() ?>theme/theme/assets/js/core/popper.min.js"></script>
<script src="<?= base_url() ?>theme/theme/assets/js/bootstrap-material-design.js"></script>
<!--  Google Maps Plugin  -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfiIZSk_QC7UWgdGekKZLEUXUBi_mWPnE"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin  -->
<script src="<?= base_url() ?>theme/theme/assets/js/plugins/moment.min.js"></script>
<!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
<script src="<?= base_url() ?>theme/theme/assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
<!--	Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="<?= base_url() ?>theme/theme/assets/js/plugins/nouislider.min.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="<?= base_url() ?>theme/theme/assets/js/plugins/bootstrap-selectpicker.js"></script>
<!--	Plugin for Tags, full documentation here: http://xoxco.com/projects/code/tagsinput/  -->
<script src="<?= base_url() ?>theme/theme/assets/js/plugins/bootstrap-tagsinput.js"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="<?= base_url() ?>theme/theme/assets/js/plugins/jasny-bootstrap.min.js"></script>
<!--	Plugin for Small Gallery in Product Page -->
<script src="<?= base_url() ?>theme/theme/assets/js/plugins/jquery.flexisel.js"></script>
<!-- Material Kit Core initialisations of plugins and Bootstrap Material Design Library -->
<!--<script src="<?= base_url() ?>theme/theme/assets/js/material-kit.js?v=2.0.3"></script>-->

<!-- Plugins for presentation and navigation  -->
<script src="<?= base_url() ?>theme/theme/assets/js/plugins/modernizr.js"></script>
<script src="<?= base_url() ?>theme/theme/assets/js/plugins/vertical-nav.js"></script>
<!-- Material Kit Core initialisations of plugins and Bootstrap Material Design Library -->
<script src="<?= base_url() ?>theme/theme/assets/js/plugins/material-kit.js?v=2.0.3"></script>
<!-- Fixed Sidebar Nav - js With initialisations For Demo Purpose, Don't Include it in your project -->
<script src="<?= base_url() ?>theme/theme/assets/js/plugins/material-kit-demo.js"></script>






<script>
    $(document).ready(function() {
      if(document.getElementById('sliderRefine')){
          var slider2 = document.getElementById('sliderRefine');
          noUiSlider.create(slider2, {
              start: [101, 790],
              connect: true,
              range: {
                  'min': [30],
                  'max': [900]
              }
          });
          var limitFieldMin = document.getElementById('price-left');
          var limitFieldMax = document.getElementById('price-right');
          slider2.noUiSlider.on('update', function(values, handle) {
              if (handle) {
                  limitFieldMax.innerHTML = $('#price-right').data('currency') + Math.round(values[handle]);
              } else {
                  limitFieldMin.innerHTML = $('#price-left').data('currency') + Math.round(values[handle]);
              }
          });
        }
    });
</script>
<!-- Calendario -->
<script>
    $(document).ready(function() {
        $(".datepicker").datetimepicker({
           format: "DD/mm/YYYY"
        })
        

        $("#google form").on('submit',function(){
          buscarImagenGoogle();
          return false;
        });

        $("#giphy form").on('submit',function(){
          buscarImagenGiphy();
          return false;
        });

        $("#youtube form").on('submit',function(){
          buscarYoutubeVideo();
            return false;
        });

        //Almacenar votos localmente
        localStorage.votos = typeof(localStorage.votos)==='undefined'?'[]':localStorage.votos;
        
    });

</script>






<script src="<?= base_url('js/uploadFile.js') ?>"></script>    
<script>
    function remoteConnection(url,data,callback){        
        return $.ajax({
            url: '<?= base_url() ?>'+url,
            data: data,
            context: document.body,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success:callback
        });
    };

    function insertar(url,datos,callback){
        <?php if(!$this->user->log): ?>
          $("#iniciar-sesion").modal('toggle');
        <?php else: ?>
            $("#result").removeClass('alert alert-danger').html('');
            $("button[type=submit]").attr('disabled',true);
            var uri = url.replace('insert','insert_validation');
            uri = uri.replace('update','update_validation');
            remoteConnection(uri,datos,function(data){
                $("button[type=submit]").attr('disabled',false);
                data = $(data).text();
                data = JSON.parse(data);
                
                if(data.success){
                  remoteConnection(url,datos,function(data){
                    data = $(data).text();
                    data = JSON.parse(data);
                    callback(data);
                  });
                }else{
                  $("#result").addClass('alert alert-danger').html(data.error_message);
                }
            });
        <?php endif ?>
    }

    function insertarUser(url,datos,callback,responseDiv){
        if(typeof(responseDiv)==='undefined'){
          responseDiv = '#result';
        }
        $("#result").removeClass('alert alert-danger').html('');
        $("button[type=submit]").attr('disabled',true);
        var uri = url.replace('insert','insert_validation');
        uri = uri.replace('update','update_validation');
        remoteConnection(uri,datos,function(data){
            $("button[type=submit]").attr('disabled',false);
            data = $(data).text();
            data = JSON.parse(data);
            
            if(data.success){
              remoteConnection(url,datos,function(data){
                data = $(data).text();
                data = JSON.parse(data);
                callback(data);
              });
            }else{
              $(responseDiv).addClass('alert alert-danger').html(data.error_message);
            }
        });
    }

     function loginShort(f){
        f = new FormData(f);
        $("#loginShortResult").html('');
        remoteConnection('main/loginUser',f,function(data){
            //Subimos foto
            data = JSON.parse(data);
            if(data.success){
                $("#iniciar-sesion").modal('toggle');

                if($("#formPublicarLista").length>0){
                  insertar = function(url,datos,callback){
                      $("#result").removeClass('alert alert-danger').html('');
                      $("button[type=submit]").attr('disabled',true);
                      var uri = url.replace('insert','insert_validation');
                      uri = uri.replace('update','update_validation');
                      remoteConnection(uri,datos,function(data){
                          $("button[type=submit]").attr('disabled',false);
                          data = $(data).text();
                          data = JSON.parse(data);
                          console.log(data);
                          if(data.success){
                            remoteConnection(url,datos,function(data){
                              data = $(data).text();
                              data = JSON.parse(data);
                              callback(data);
                            });
                          }else{
                            $("#result").addClass('alert alert-danger').html(data.error_message);
                          }
                      });
                  }

                  $("#formPublicarLista").submit();
                }else{
                  document.location.reload();
                }                
            }else{
                $("#loginShortResult").html(data.message);
            }
        });
        return false;
    }
    function registrar(f){
        f = new FormData(f);
        //Subir foto antes de enviar formulario
        remoteConnection('registro/index/upload_file/foto',f,function(data){
            //Subimos foto
            data = JSON.parse(data);
            if(data.success && data.files.length>0){
                f.append('foto',data.files[0].name);
                remoteConnection('registro/index/insert_validation',f,function(data){
                    data = $(data).text();
                    data = JSON.parse(data);
                    if(data.success){
                        remoteConnection('registro/index/insert',f,function(data){
                            data = $(data).text();
                            data = JSON.parse(data);
                            if(data.success){
                                document.location.href="<?= base_url('panel') ?>";
                            }else{
                                $("#registroResult").html('<div class="alert alert-danger">Ocurrio un error al añadir usuario</div>');
                            }
                        });
                    }else{
                        $("#registroResult").html('<div class="alert alert-danger">'+data.error_message+'</div>');
                    }
                });
            }else{
                $("#registroResult").html('<div class="alert alert-danger">Por favor adjunte una fotografia</div>');
            }
        });
        return false;
    } 

    var activeContent = undefined;    
    var emptyImg = URL+'theme/theme/assets/img/Perfiles/perfil.jpg';
    function setInputImagenLista(elemento){
      activeContent = $(elemento).parents('.contenedor-tarjeta-redeem');      
      $(".busquedaAdjunto").val('');
      $(".contendor-buscador-imagenes > div").html('');
      $(".busquedaNav").on('click',function(){
        $(".busquedaAdjunto").val('');
        $(".contendor-buscador-imagenes > div").html('');
      });
    }

    function buscarImagenGoogle(){      
      if(activeContent.find('.adjunto').val()!==''){
        $("#removeUploadFileLista").trigger('click');
      }
      remoteConnection('listas/ShortCodes/buscarImagenGoogle?q='+$("#buscarImagenGoogle").val(),new FormData(),function(data){
          $("#buscarImagenGoogleResponse").html(data);
      });
      return false;
    }

    function buscarImagenGiphy(){
      search = $("#buscarImagenGiphy").val();
      if(search===''){
        return '';
      }
      var url = 'https://api.giphy.com/v1/gifs/search?api_key=oM9f3hXX0U02UVQDq2UxBuvG5STlGUqg&q='+search+'&limit=25&offset=0&rating=G&lang=en';
      console.log(url);
      responseContent = $("#buscarImagenGifResponse");
      responseContent.html('');
      $.getJSON(url).done(function (data) {          
          if(data.data.length>0){
            var str = '<h1>Resultados</h1> <hr> <div class="row">';
            for(var i in data.data){
              str+= '<div class="col-xs-12 col-sm-6"><a href="javascript:seleccionarImagenGoogle(\''+data.data[i].images.original.url+'\')"><img src="'+data.data[i].images.original.url+'" style="width:100%;"></a></div>';
            }
            str+= '</div>';
          }
          responseContent.html(str);
      });      
    }

    function buscarYoutubeVideo(){
      search = $("#buscarYoutubeVideo").val();
      if(search===''){
        return '';
      }
      responseContent = $("#buscarYoutubeVideoResponse");
      responseContent.html('');
      var url = 'https://www.googleapis.com/youtube/v3/search?part=id,snippet&q='+search+'&key=AIzaSyBiTQp5QUWTeD0XQpNnv3kevaKrtokkpnA';
      $.getJSON(url).done(function (data) {                 
          if(data.items.length>0){
            console.log(data);
            var str = '<h1>Resultados</h1> <hr> <div class="row">';
             for(var i in data.items){
              var video = data.items[i].id;
              if(video.kind==='youtube#video'){
                str+= '<div class="col-xs-12">';
                str+= '<div class="videoTitle">Video: '+data.items[i].snippet.title+'</div>';
                str+= '<div class="videbutton"><a href="javascript:seleccionarYoutubeVideo(\''+data.items[i].snippet.thumbnails.high.url+'\',\''+video.videoId+'\')" class="btn btn-info">Seleccionar</a></div>';
                str+= '<div class="videoFrame"><iframe height="315" style="width:100%;" src="https://www.youtube.com/embed/'+video.videoId+'?rel=0" frameborder="0"></iframe></div>';
                str+= '</div>';
              }
            }
            str+= '</div>';
          }
          responseContent.html(str);
      });      

    }

    function seleccionarImagenGoogle(url){
      $("#fileAdjuntLista").attr('src',url);      
      activeContent.find('img').attr('src',url);
      activeContent.find('.adjunto').val(url);
      activeContent.find('.tipo_adjunto').val('');
    }

    function seleccionarYoutubeVideo(url,idVideo){
      $("#fileAdjuntLista").attr('src',url);
      activeContent.find('img').attr('src',url);
      activeContent.find('.adjunto').val(url);
      activeContent.find('.tipo_adjunto').val('youtube|'+idVideo);
      activeContent.find('.autor').val('youtube.com');
    }

    $(function () {
      'use strict';
      $('#uploadImageLista').fileupload({
          url: '<?= base_url() ?>listas/frontend/upload_file',          
          done: function (e, data) {
             //Capturamos el nombre de la imagen 
             var name = data.result;
             $("#fileAdjuntLista").attr('src',URL+'img/listas/'+name);
             activeContent.find('img').attr('src',URL+'img/listas/'+name);
             activeContent.find('.adjunto').val(name);
             activeContent.find('.tipo_adjunto','img_local');
             $("#progressUploadFileLista").hide();
          },
          progressall: function (e, data) {
              var progressbar = $("#progressUploadFileLista");
              progressbar.show();
              var progress = parseInt(data.loaded / data.total * 100, 10);
              $(progressbar).css(
                  'width',
                  progress + '%'
              );
              $(progressbar).html(progress + '%');
          }
      });

      $("#removeUploadFileLista").on('click',function(){
        eraseUploadFile();
      });
    });

    function eraseUploadFile(){
      $.get(URL+'listas/frontend/eraseFile/'+activeContent.find('.adjunto').val(),{},function(){
        $("#fileAdjuntLista").attr('src',emptyImg);
        activeContent.find('img').attr('src',emptyImg);
        activeContent.find('.adjunto').val('');
        activeContent.find('.tipo_adjunto').val('');        
      });
    }

    $(document).on('change','.categorias_id',function(){
        if($(this).val()!==''){
          var contenedor = $(this).parents('.participantRow');
          $.post(URL+'listas/frontend/listas_subcategorias/json_list',{
            'search_field[]':'categorias_id',
            'search_text[]':$(this).val()
          },function(data){
            var data = JSON.parse(data);
            var str = '<option value="">Seleccione una subcategoria</option>';
            for(var i in data){
              str+= '<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
            }
            contenedor.find(".subcategorias_id").html(str);
          });
        }
    });

    $(document).on('click','.adjuntoPreview',function(e){
        e.preventDefault();
        var contenido = $(this).parents('.contenedor-tarjeta-redeem');
        var tipo_adjunto = contenido.find('.tipo_adjunto').val();
        var url = contenido.find('.adjunto').val();
        if(url!==''){
          if(tipo_adjunto.indexOf('youtube')>-1){
            var video = tipo_adjunto.split('|');
            var img = '<iframe height="315" style="width:100%;" src="https://www.youtube.com/embed/'+video[1]+'?rel=0" frameborder="0"></iframe>';
          }else{
            var img = '<img src="'+url+'" style="width:100%;">';
          }
          console.log(tipo_adjunto);
          $("#verAdjuntoModal .modal-body").html(img);
          $("#verAdjuntoModal").modal('toggle');

          $("#verAdjuntoModal").on('hidden.bs.modal',function(e){
            $("#verAdjuntoModal .modal-body").html('');
          });
        }
    });

    



    function comparar(id){
        var content = $("#listavslista");
        content.modal('toggle');
        content.find('.modal-body').html('Consultando información, por favor espere...');
        $.post(URL+'listas/frontend/comparador/'+id,{},function(data){
          $(".modal-body").html(data);
        });        
    }
</script>






<?php if(!empty($js_files)): ?>      
  <?php foreach($js_files as $file): ?>
    <script src="<?= $file ?>"></script>
  <?php endforeach; ?>                
<?php endif; ?>

<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
  $(".registrofoto").change(function(){
      
  });

  $("#campana .dropdown-toggle").on('click',function(){

    $.get(URL+'mensajeria',{},function(){
      $("#campana .badge").html('');
    });
  });
</script>