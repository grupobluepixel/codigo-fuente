<div class="container-fluid contenedor-ads fondo-gradient-gris-oscuro">
    <div class="container">

        <div class="row">
            <h2 class="titulo-gastronomia-categorias texto-blanco">
                <img src="http://labtico.com/img/categorias/7ec7b-icono-entretenimiento.png">Entretenimiento y Humor
            </h2>
        </div>

        <div class="row">
          <!-- Tarjeta -->
          <div class="col-sm-3">
              <div class="table-responsive tabla-categorias">
                      <table class="table table-striped">
                          <thead class="titulo-categorias">
                              <div class="media header-categoria">
                                  <div class="imagen-categorias center-block">
                                      <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                  </div>
                                  <div class="media-body"><b>Gastronomía y<br>Restaurantes<br><span style="color:#3FA0E8;">#100</span></b></div>
                              </div>
                          </thead>
                          <tbody>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td><a href="<?= base_url() ?>categorias-interna.html">Animales y Mascotas</a></td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td><a href="<?= base_url() ?>categorias-interna.html">Animales y Mascotas</a></td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td><a href="<?= base_url() ?>categorias-interna.html">Animales y Mascotas</a></td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td><a href="<?= base_url() ?>categorias-interna.html">Animales y Mascotas</a></td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td><a href="<?= base_url() ?>categorias-interna.html">Animales y Mascotas</a></td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
          </div>
          <!-- Tarjeta -->
          <div class="col-sm-3">
              <div class="table-responsive tabla-categorias">
                      <table class="table table-striped">
                          <thead class="titulo-categorias">
                              <div class="media header-categoria">
                                  <div class="imagen-categorias center-block">
                                      <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                  </div>
                                  <div class="media-body"><b>Gastronomía y<br>Restaurantes<br><span style="color:#3FA0E8;">#100</span></b></div>
                              </div>
                          </thead>
                          <tbody>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
          </div>
          <!-- Tarjeta -->
          <div class="col-sm-3">
              <div class="table-responsive tabla-categorias">
                      <table class="table table-striped">
                          <thead class="titulo-categorias">
                              <div class="media header-categoria">
                                  <div class="imagen-categorias center-block">
                                      <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                  </div>
                                  <div class="media-body"><b>Gastronomía y<br>Restaurantes<br><span style="color:#3FA0E8;">#100</span></b></div>
                              </div>
                          </thead>
                          <tbody>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
          </div>
          <!-- Tarjeta -->
          <div class="col-sm-3">
              <div class="table-responsive tabla-categorias">
                      <table class="table table-striped">
                          <thead class="titulo-categorias">
                              <div class="media header-categoria">
                                  <div class="imagen-categorias center-block">
                                      <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                  </div>
                                  <div class="media-body"><b>Gastronomía y<br>Restaurantes<br><span style="color:#3FA0E8;">#100</span></b></div>
                              </div>
                          </thead>
                          <tbody>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
          </div>
        </div>

    </div>
</div>

<div class="container-fluid contenedor-ads fondo-gradient-gris">
    <div class="container">

        <div class="row">
            <h2 class="titulo-gastronomia-categorias texto-blanco">
                <img src="http://labtico.com/img/categorias/7ec7b-icono-entretenimiento.png"> Gastronomía y restaurantes
            </h2>
        </div>

        <div class="row">
          <!-- Tarjeta -->
          <div class="col-sm-3">
              <div class="contenedor-tarjeta-blanco fondo-blanco">
                  <div class="table-responsive tabla-categorias">
                      <table class="table table-striped">
                          <thead class="titulo-categorias">
                              <div class="media header-categoria">
                                  <div class="imagen-categorias center-block">
                                      <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                  </div>
                                  <div class="media-body"><b>Gastronomía y<br>Restaurantes<br><span style="color:#3FA0E8;">#100</span></b></div>
                              </div>
                          </thead>
                          <tbody>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
          <!-- Tarjeta -->
          <div class="col-sm-3">
              <div class="contenedor-tarjeta-blanco fondo-blanco">
                  <div class="table-responsive tabla-categorias">
                      <table class="table table-striped">
                          <thead class="titulo-categorias">
                              <div class="media header-categoria">
                                  <div class="imagen-categorias center-block">
                                      <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                  </div>
                                  <div class="media-body"><b>Gastronomía y<br>Restaurantes<br><span style="color:#3FA0E8;">#100</span></b></div>
                              </div>
                          </thead>
                          <tbody>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
          <!-- Tarjeta -->
          <div class="col-sm-3">
              <div class="contenedor-tarjeta-blanco fondo-blanco">
                  <div class="table-responsive tabla-categorias">
                      <table class="table table-striped">
                          <thead class="titulo-categorias">
                              <div class="media header-categoria">
                                  <div class="imagen-categorias center-block">
                                      <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                  </div>
                                  <div class="media-body"><b>Gastronomía y<br>Restaurantes<br><span style="color:#3FA0E8;">#100</span></b></div>
                              </div>
                          </thead>
                          <tbody>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
          <!-- Tarjeta -->
          <div class="col-sm-3">
              <div class="contenedor-tarjeta-blanco fondo-blanco">
                  <div class="table-responsive tabla-categorias">
                      <table class="table table-striped">
                          <thead class="titulo-categorias">
                              <div class="media header-categoria">
                                  <div class="imagen-categorias center-block">
                                      <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                  </div>
                                  <div class="media-body"><b>Gastronomía y<br>Restaurantes<br><span style="color:#3FA0E8;">#100</span></b></div>
                              </div>
                          </thead>
                          <tbody>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
        </div>

    </div>
</div>


<div class="container-fluid contenedor-ads fondo-gradient-gris-oscuro">
    <div class="container">

        <div class="row">
            <h2 class="titulo-gastronomia-categorias texto-blanco">
                <img src="http://labtico.com/img/categorias/7ec7b-icono-entretenimiento.png"> Musica
            </h2>
        </div>

        <div class="row">
          <!-- Tarjeta -->
          <div class="col-sm-3">
              <div class="contenedor-tarjeta-blanco fondo-blanco">
                  <div class="table-responsive tabla-categorias">
                      <table class="table table-striped">
                          <thead class="titulo-categorias">
                              <div class="media header-categoria">
                                  <div class="imagen-categorias center-block">
                                      <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                  </div>
                                  <div class="media-body"><b>Gastronomía y<br>Restaurantes<br><span style="color:#3FA0E8;">#100</span></b></div>
                              </div>
                          </thead>
                          <tbody>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
          <!-- Tarjeta -->
          <div class="col-sm-3">
              <div class="contenedor-tarjeta-blanco fondo-blanco">
                  <div class="table-responsive tabla-categorias">
                      <table class="table table-striped">
                          <thead class="titulo-categorias">
                              <div class="media header-categoria">
                                  <div class="imagen-categorias center-block">
                                      <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                  </div>
                                  <div class="media-body"><b>Gastronomía y<br>Restaurantes<br><span style="color:#3FA0E8;">#100</span></b></div>
                              </div>
                          </thead>
                          <tbody>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
          <!-- Tarjeta -->
          <div class="col-sm-3">
              <div class="contenedor-tarjeta-blanco fondo-blanco">
                  <div class="table-responsive tabla-categorias">
                      <table class="table table-striped">
                          <thead class="titulo-categorias">
                              <div class="media header-categoria">
                                  <div class="imagen-categorias center-block">
                                      <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                  </div>
                                  <div class="media-body"><b>Gastronomía y<br>Restaurantes<br><span style="color:#3FA0E8;">#100</span></b></div>
                              </div>
                          </thead>
                          <tbody>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
          <!-- Tarjeta -->
          <div class="col-sm-3">
              <div class="contenedor-tarjeta-blanco fondo-blanco">
                  <div class="table-responsive tabla-categorias">
                      <table class="table table-striped">
                          <thead class="titulo-categorias">
                              <div class="media header-categoria">
                                  <div class="imagen-categorias center-block">
                                      <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                  </div>
                                  <div class="media-body"><b>Gastronomía y<br>Restaurantes<br><span style="color:#3FA0E8;">#100</span></b></div>
                              </div>
                          </thead>
                          <tbody>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                              <tr>
                                  <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                  <td>Animales y Mascotas</td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
        </div>

    </div>
</div>



<!--
    <div class="container-fluid contenedor-ads">
        <div class="container">
            <div class="row margin-categorias-tarjetas">
                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                      <td>Animales y Mascotas</td>
                                  </tr>
                                  <tr>
                                      <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                      <td>Animales y Mascotas</td>
                                  </tr>
                                  <tr>
                                      <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                      <td>Animales y Mascotas</td>
                                  </tr>
                                  <tr>
                                      <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                      <td>Animales y Mascotas</td>
                                  </tr>
                                  <tr>
                                      <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                      <td>Animales y Mascotas</td>
                                  </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>
            </div>

            <div class="row margin-categorias-tarjetas">
                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>
            </div>


            <div class="row margin-categorias-tarjetas">
                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>
            </div>

        </div>
    </div>-->

    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php include('footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php include('footer-mapa.php');?>
    </footer>

    <!-- Modales -->
    <?php include('modales.php');?>

    <!-- Librerias -->
    <?php include('librerias.php');?>
