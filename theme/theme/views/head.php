<link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,700,700i,900,900i" rel="stylesheet">
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
<link rel="stylesheet" href="<?= base_url() ?>theme/theme/views/assets/css/material-kit.css?v=2.0.3">
<link rel="stylesheet" href="<?= base_url() ?>theme/theme/views/assets/css/main.css">
<link rel="stylesheet" href="<?= base_url() ?>theme/theme/views/assets/css/queries.css">
<link rel="stylesheet" href="<?= base_url() ?>theme/theme/views/assets/css/bootstrap.css">
<!-- <?= base_url() ?>theme/theme/views/Slider -->
<link rel="stylesheet" href="<?= base_url() ?>theme/theme/views/Slider/owl.carousel.css">
<link rel="stylesheet" href="<?= base_url() ?>theme/theme/views/Slider/owl.theme.default.min.css">

<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>

<!-- CSS Just for demo purpose, don't include it in your project -->
<link rel="stylesheet" href="<?= base_url() ?>theme/theme/views/assets/css/demo.css"/>
<link rel="stylesheet" href="<?= base_url() ?>theme/theme/views/assets/css/vertical-nav.css" />

<script src="<?= base_url() ?>theme/theme/views/Slider/jquery.min.js"></script>
<script src="<?= base_url() ?>theme/theme/views/Slider/owl.carousel.js"></script>

<!-- Owl Stylesheets -->
<link rel="stylesheet" href="<?= base_url() ?>theme/theme/views/Slider/owl.carousel.min.css">
<link rel="stylesheet" href="<?= base_url() ?>theme/theme/views/Slider/owl.theme.default.min.css">
<!-- Drag & Drop -->
<link href='<?= base_url() ?>theme/theme/views/Drag-drop/example.css' rel='stylesheet' type='text/css' />
