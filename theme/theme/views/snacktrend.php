<div class="contenedor-snacktrend text-center">
    <div class="row logo-snacktrend">
        <div class="col-sm-12">
          <a href="<?= base_url('snacktrend') ?>">
            <img src="<?= base_url() ?>theme/theme/assets/img/logo-snacktrend.png" alt="Logo Snacktrend" class="center-block img-responsive">
          </a>
        </div>
    </div>

    <?php
        $this->db->order_by('votos','DESC');
        $this->db->limit(10);
        $trends = $this->querys->get_listas(array());
    ?>
    <!-- TOP 4 -->
    <div class="row texto-blanco padding30-bottom grid">
        <div class="col-sm-12">
        <?php if($trends->num_rows()>0): ?>
          <?php for($i=0;$i<4;$i++): if($i<$trends->num_rows()): $t = $trends->row($i); ?>
              <div class="col-xs-6 col-md-3 texto-blanco">
                  <div class="contenedor-tarjeta-snacktrend">

                      <figure class="snip1585">
                        
                        <div class="img-snactrend-fondo" style="background:url(<?= $this->querys->get_foto_primaria($t->id) ?>); background-size:cover; width:100%; height:180px;">
                            <div class="titulo-header-tarjtea-snacktrend">
                                <div class="posicion-snacktrend"><?= $i+1 ?></div>
                                <div class="perfil-snacktrend">
                                  <img src="<?= $t->perfil ?>" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
                                </div>
                                <div class="username-snacktrend"><?= $t->username ?><br><b>Votos:<?= $t->votos ?></b></div>
                            </div>
                            <img src="<?= $this->querys->get_foto_primaria($t->id) ?>" alt="<?= $t->titulo ?>" style="display:none;" />
                        </div>

                        <figcaption>
                            <h3 class="btns-votar-snacktrend">
                                <a href="<?= $t->link ?>" class="btn-ver-snacktrend-vistas"><i class="fa fa-eye" aria-hidden="true"></i> <?= $t->vistas ?></a>
                            </h3>
                            <h3 class="btns-votar-snacktrend">
                                <a href="<?= $t->link ?>" class="btn-ver-snacktrend">Votar/Reordenar</a>
                            </h3>
                            <h3 class="vistas-snacktrend">
                                <div class="ver-snacktrend" title="Ver Lista"><a href="<?= $t->link ?>"><i class="material-icons">redo</i></a></div>
                            </h3>
                        </figcaption>

                      </figure>

                      <div class="footer-tarjeta-snacktrend">
                          <h3>
                            <span class="categoria-snacktrend">
                              <?= $t->catnom ?> / <?= $t->subcatnom ?>
                            </span>
                            <br><?= $t->titulo_corto ?>
                          </h3>
                      </div>
                  </div>
              </div>
              <?php endif ?>
          <?php endfor ?>
        <?php endif ?>

    </div>
  </div>

    <!-- TOP 6 -->
    <div class="row texto-blanco">
        <div class="col-sm-12">

        <?php if($trends->num_rows()>4): ?>
          <?php for($i=4;$i<10 && $i<$trends->num_rows();$i++): $t = $trends->row($i); ?>
            <div class="col-xs-6 col-md-2 texto-blanco">
                <div class="contenedor-tarjeta-snacktrend">
                    <figure class="snip1585">

                      <div class="titulo-header-tarjtea-snacktrend">
                          <div class="posicion-snacktrend"><?= $i+1 ?></div>
                          <div class="perfil-snacktrend-top6"><img src="<?= $t->perfil ?>" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-snacktrend"><?= $t->username ?><br><b>Votos:<?= $t->votos ?></b></div>
                      </div>

                      <div class="img-snactrend-fondo-top6">
                          <img src="<?= $this->querys->get_foto_primaria($t->id) ?>" alt="<?= $t->titulo ?>" />
                      </div>

                      <figcaption>
                          <h3 class="btns-votar-snacktrend">
                              <a href="<?= $t->link ?>" class="btn-ver-snacktrend-vistas-top6">
                                  <i class="fa fa-eye" aria-hidden="true"></i> <?= $t->vistas ?>
                              </a>
                          </h3>
                          <h3 class="btns-votar-snacktrend">
                              <a href="<?= $t->link ?>" class="btn-ver-snacktrend-top6">Votar/Reordenar</a></button>
                          </h3>
                          <h3 class="vistas-snacktrend">
                              <div class="ver-snacktrend">
                                  <a href="<?= $t->link ?>"><i class="material-icons">redo</i></a>
                              </div>
                          </h3>
                      </figcaption>

                      <a href="<?= $t->link ?>"></a>
                    </figure>
                    <div class="footer-tarjeta-snacktrend-top6">
                        <h3>
                          <span class="categoria-snacktrend">
                            <?= $t->catnom ?> / <?= $t->subcatnom ?>
                          </span>
                          <br><?= $t->titulo_corto ?></h3>
                    </div>
                </div>
            </div>
          <?php endfor ?>
        <?php endif ?>
    </div>

  </div>
</div>

<script>
/* Demo purposes only */
$(".hover").mouseleave(
  function() {
    $(this).removeClass("hover");
  }
);
</script>
