    <div class="container-fluid container-fluid fondo-gradient-gris contenedor-listado-tarjetas" style="padding-top:110px">
        <div class="container">
              <?php if(!empty($miscategorias)): ?>
              <?php if($miscategorias->num_rows()>0): ?>
              <div class="row">
                  <div class="col-sm-12">
                      <b>Las listas aparecerán por categoría en el orden cronológico en el que fueron creadas.<br>
                      Puedes ver tu lista original vs con la lista actualizada dando click en <a href="#">comparar</a> </b>
                  </div>

                  <div class="col-sm-12 menu-listas" >
                    <!--Menu Mis listas por categoría -->
                    <nav class="navbar navbar-expand-lg navbar-light" id="nav-listas">
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav">
                              <?php foreach($miscategorias->result() as $c): ?>
                                <li class="nav-item">
                                  <a class="nav-link" href="#cat<?= $c->id ?>"><b><?= $c->nombre ?></b></a>
                                </li>
                              <?php endforeach ?>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
  
            <?php foreach($miscategorias->result() as $c): ?>
            <?= $c->output ?>
            <?php endforeach ?>
            <?php else: ?>
              Parece que aún no has registrado ninguna lista, te invitamos a hacerlo en la sección de <a href="<?= base_url('publicar-lista') ?>">publicar listas</a>
            <?php endif ?>
            <?php else: ?>
              <?= $output ?>
            <?php endif ?>
        </div>
    </div>

    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php include('footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php include('footer-mapa.php');?>
    </footer>

    <!-- Modales -->
    <?php include('modales.php');?>

    <!-- Librerias -->
    <?php include('librerias.php');?>

    <!-- Select Input -->
    <script>
      $('#nav-listas a').click(function(e){
      e.preventDefault();   //evitar el eventos del enlace normal
      var strAncla=$(this).attr('href'); //id del ancla
        $('body,html').stop(true,true).animate({
          scrollTop: $(strAncla).offset().top-20
        },1000);

    });
    </script>

    <!-- Add / Remove -->
    <script>
    /* Variables */
    var p = $("#participants").val();
    var row = $(".participantRow");

    /* Functions */
    function getP(){
    p = $("#participants").val();
    }

    function addRow() {
    row.clone(true, true).appendTo("#participantTable");
    }

    function removeRow(button) {
    button.closest("div").remove();
    }
    /* Doc ready */
    $(".add").on('click', function () {
    getP();
    if($("#participantTable div").length < 17) {
      addRow();
      var i = Number(p)+1;
      $("#participants").val(i);
    }
    $(this).closest("tr").appendTo("#participantTable");
    if ($("#participantTable div").length === 3) {
      $(".remove").hide();
    } else {
      $(".remove").show();
    }
    });
    $(".remove").on('click', function () {
    getP();
    if($("#participantTable div").length === 3) {
      //alert("Can't remove row.");
      $(".remove").hide();
    } else if($("#participantTable div").length - 1 ==3) {
      $(".remove").hide();
      removeRow($(this));
      var i = Number(p)-1;
      $("#participants").val(i);
    } else {
      removeRow($(this));
      var i = Number(p)-1;
      $("#participants").val(i);
    }
    });
    $("#participants").change(function () {
    var i = 0;
    p = $("#participants").val();
    var rowCount = $("#participantTable div").length - 2;
    if(p > rowCount) {
      for(i=rowCount; i<p; i+=1){
        addRow();
      }
      $("#participantTable #addButtonRow").appendTo("#participantTable");
    } else if(p < rowCount) {
    }
    });
    </script>
</body>
</html>
