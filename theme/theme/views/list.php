  <?php foreach ($list as $num_row =>$row): ?>    

    <!-- Tarjeta -->
    <div class="col-sm-3">
      <div class="contenedor-tarjeta-blanco fondo-blanco">

        <div class="col-sm-12 texto-gris-oscuro text-left">
            <h3>
              <a href="<?= base_url('categoria/'.toUrl($row->catid.'-'.$row->categoria)) ?>">
                  <span class="categoria-snacktrend"><?= $row->categoria ?></span>
              </a>
            </h3>
        </div>


        <a href="<?= base_url('lista/'.toUrl($row->id.'-'.$row->titulo_corto)) ?>">
            <div class="col-sm-12">
                <div class="header-tarjeta-categoria texto-gris-oscuro">
                    <h3><?= $row->titulo_corto ?></h3>
                </div>
                <?php get_instance()->db->limit(3); ?>
                <?php foreach(get_instance()->db->get_where('listas_detalles',array('listas_id'=>$row->id))->result() as $detall): ?>
                  <div class="contenedor-listado-categoria">
                    <div class="imagen-listado-tarjeta" style="background:url(<?= get_instance()->querys->get_foto($detall->adjunto) ?>); background-position:center center; background-size:cover;">
                      <div class="top-listado-tarjeta texto-blanco"><?= $detall->posicion ?></div>
                    </div>
                    <div class="listado-tarjeta"><b><?= $detall->nombre ?></b><br><?= $detall->autor ?></div>
                  </div>
                <?php endforeach ?>
                
            </div>
        </a>        


        <div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
            <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                <!--
                <div class="perfil-tarjeta-categorias">
                    <img src="<?= get_instance()->querys->get_perfil(get_instance()->user->foto) ?>" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
                </div>-->
                <div class="username-tarjetas"><b><?= $row->se8701ad4 ?></b><br>Votos:<?= $row->votos ?></div>
            </div>
        </div>


        <a href="javascript:comparar(<?= $row->id ?>);">
            <div class="col-xs-12 col-sm-12 text-center btn-comparar" style="margin-top: 5px;">
                  <!--<a href="<?= base_url() ?>analisis/<?= toUrl($row->id.'-'.$row->titulo) ?>" class="btn-menu-top"><b>Comparar</b></a>-->
                  <b>Comparar</b>
            </div>
        </a>

      </div>
    </div>

  <?php endforeach ?>
</div>






<!-- Menu -->
<script>

if(document.getElementById("myHeader")){
    window.onscroll = function() {myFunction()};
    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;
    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else if(header) {
        header.classList.remove("sticky");
      }
    }
}
</script>
