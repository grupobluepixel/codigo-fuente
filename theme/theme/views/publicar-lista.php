
    <div class="container-fluid fondo-blanco margin-secciones-menu-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 contenedor-ads">Google ADS</div>
            </div>
        </div>
    </div>

    <form id="formPublicarLista" action="" onsubmit="return guardarLista(this)" method="post">
        <div class="container-fluid contenedor-snackteam fondo-blanco">
            <div class="container">

                <div class="row-padding">
                    <div class="row titulo-redeem texto-negro"><h2>Publicar nueva lista</h2></div>

                    <div class="row">
                        <div class="col-sm-12 texto-input-publicar">140 caracteres | Se verá en la lista completa</div>
                        <input class="form-control margin-login-input" id="input-publicar" name="titulo" placeholder="Título de la lista" maxlength="140" type="text" style="font-weight: 900;color: #3FA0E8;">
                        <div class="col-sm-12 texto-input-publicar">50 caracteres | Se verá como preview de la lista</div>
                        <input class="form-control margin-login-input" id="input-publicar" name="titulo_corto" placeholder="Título corto" maxlength="50" type="text">
                    </div>

                    <div class="row titulo-redeem texto-negro"><h3><b>Clasificación</b></h3></div>

                    


                    <!-- Prueba -->
                    <div class="row contenedor-select-publicar">
                        <div class="col-sm-12 padding0" id="participantTable" style="padding-right:0px; padding-left:0px;">
                            <div class="participantRow">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <select id="selectbasic" name="categorias_id[]" class="form-control select-publicar categorias_id">
                                            <option value="" id="input-publicar">Categoría</option>
                                            <?php $this->db->order_by('nombre','ASC'); foreach($this->db->get_where('categorias',array())->result() as $b): ?>
                                            <option value="<?= $b->id ?>"><?= $b->nombre ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <select id="selectbasic2" name="sub_categorias_id[]" class="form-control select-publicar subcategorias_id">
                                            <option value="" id="input-publicar">Subcategoría</option>
                                            <?php $this->db->order_by('nombre','ASC');  foreach($this->db->get_where('sub_categorias',array())->result() as $b): ?>
                                                    <option value="<?= $b->id ?>"><?= $b->nombre ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <button class="remove btn-remover-categoria" type="button" title="Remover"><i class="material-icons">close</i></button>
                                    <button class="add btn-agregar-categoria" type="button" title="Agregar una categoría y subcategoría"><i class="material-icons">add</i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Prueba -->

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 texto-input-publicar-recomendacion">
                            Te recomendamos que antes de publicar tu lista revises en el buscador que no exista alguna lista con el mismo título, en caso de que si exista podrás votar y/o reordenarla.<br>
                            Arrastra cualquier elemento para reposicionarlo
                        </div>
                        <div class="col-xs-12 col-sm-12 contenedor-tabs-redeem">
                            <div class="col-xs-12 col-sm-12 text-center" style="color:#5A90E6">*Los 3 primeros lugares son obligatorios</div>

                            <div class='parent'>
                              <div class='wrapper' id="reordenar">
                                <div id='left-rollbacks' class='container-drag'>

                                    <?php for($i=0;$i<10;$i++): ?>
                                        <!-- Tarjeta publicar -->
                                        <div class="col-xs-12 col-sm-12 contenedor-tarjeta-redeem <?= $i<3?'lista-primeros-lugares':'' ?>">
                                            <div class="col-xs-12 col-sm-1 col-md-1 numero-votar">
                                                <i class="material-icons">arrow_drop_up</i><br>
                                                <span class="indice"><?= $i+1 ?></span><br>
                                                <i class="material-icons">arrow_drop_down</i>
                                            </div>

                                            <div class="col-xs-12 col-sm-8 col-md-8">
                                                <div class="col-xs-12 col-sm-12">
                                                    <input class="form-control margin-login-input" id="input-publicar-lista" name="detalles[<?= $i ?>][nombre]" placeholder="Nombre del elemento" type="text" data-name="nombre">
                                                </div>

                                                <div class="col-xs-12 col-sm-12">
                                                    <input class="form-control margin-login-input autor" id="input-publicar-lista"  name="detalles[<?= $i ?>][autor]" placeholder="Autor de la imagen" type="text" data-name="autor">
                                                    <input type="hidden" id="adjunto<?= $i ?>" class="adjunto" name="detalles[<?= $i ?>][adjunto]" value="" data-name="adjunto">
                                                    <input type="hidden" id="tipo_adjunto<?= $i ?>" class="tipo_adjunto" name="detalles[<?= $i ?>][tipo_adjunto]" value="" data-name="tipo_adjunto">
                                                </div>
                                            </div>

                                            <div class="col-xs-6 col-sm-2 col-md-2 imagen-default-lista">
                                                <div rel="tooltip" onclick="setInputImagenLista(this)" data-original-title="" data-toggle="modal" data-target="#seleccionar-foto-lista" title="Agregar una imagen, video o gif">
                                                    <img id="previewLista<?= $i ?>" src="<?= base_url() ?>theme/theme/assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem">
                                                </div>
                                                <a href="#" class="adjuntoPreview" title="Preview">
                                                    <div class="ver-preview"><i class="material-icons">remove_red_eye</i></div>
                                                </a>
                                            </div>

                                            <div class="col-xs-6 col-sm-1 col-md-1 icon-redeem">
                                                <div class="iconos-publicar iconos-publicar-lista" rel="tooltip" onclick="$(this).parent().parent().find('input').val(''); $(this).parent().parent().find('img').attr('src','<?= base_url() ?>theme/theme/assets/img/Perfiles/perfil.jpg')"><i class="material-icons">delete_forever</i></div>
                                                <!--<div class="iconos-publicar iconos-publicar-lista" rel="tooltip" onclick="$(this).parent().parent().remove()"><i class="material-icons">delete_forever</i></div>-->
                                                <!--<div class="iconos-publicar iconos-publicar-lista" rel="tooltip" onclick="$(this).parent().parent().find('input').val(''); $(this).parent().parent().find('img').attr('src','<?= base_url() ?>theme/theme/assets/img/Perfiles/perfil.jpg')" title="Limpiar"><i class="material-icons">replay</i></div>-->
                                            </div>

                                        </div>
                                    <?php endfor ?>




                                </div><!-- Termina div Drag & Drop -->
                              </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div id="result"></div>
                        </div>
                        <div class="col-sm-12 contenedor-tabs-redeem">
                          <button type="submit" style="background-color:transparent;"><div class="col-sm-6 text-center btn-menu-top btn-general btn-derecha pull-right"><b>Publica tu lista</b></div></button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>

    <!-- Recomendaciones
    <div class="container-fluid fondo-azul-claro">
        <div class="container">
              <div class="row">
                  <?php //include('recomendaciones.php');?>
              </div>
        </div>
    </div>-->


    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php include('footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php include('footer-mapa.php');?>
    </footer>

    <!-- Librerias -->
    <?php include('librerias.php');?>
    <!-- Modales -->
    <?php include('modales.php');?>


    <!-- Add / Remove -->
    <script>
    /* Variables */


    function guardarLista(form){
        var f = new FormData(form);
        insertar('listas/frontendAdmin/listas/insert',f,function(data){
            if(data.success){
                $("#result").addClass('alert alert-success').html('Se ha almacenado su lista con éxito');
                setTimeout(function(){
                    document.location.href="<?= base_url() ?>";
                },3000);
            }
        });
        return false;
    }

    function ordenarIndex(){
        var x = 0;
        $("#reordenar .contenedor-tarjeta-redeem").each(function(){
            $(this).find('input').each(function(){
                var name = $(this).data('name');
                $(this).attr('name','detalles['+x+']['+name+']');            
            });
            $(this).find('.indice').html(x+1);
            console.log(x);
            x++;
        })
      }





    var p = $("#participants").val();
    var row = $(".participantRow");

    /* Functions */
    function getP(){
    p = $("#participants").val();
    }

    function addRow() {

        row.clone(true, true).appendTo("#participantTable");
    }

    function removeRow(button) {
        if($(".participantRow").length > 1) {
            button.parents(".participantRow").remove();
        }

    }
    /* Doc ready */
    $(".add").on('click', function () {
    getP();
    if($(".participantRow").length < 2) {

      addRow();
      var i = Number(p)+1;
      $("#participants").val(i);
    }
    $(this).closest("tr").appendTo("#participantTable");
    if ($("#participantTable div").length === 3) {
      $(".remove").hide();
    } else {
      $(".remove").show();
    }
    });
    $(document).on('click',".remove", function () {
    getP();
    if($("#participantTable div").length === 3) {
      //alert("Can't remove row.");
      $(".remove").hide();
    } else if($("#participantTable div").length - 1 ==3) {
      $(".remove").hide();
      removeRow($(this));
      var i = Number(p)-1;
      $("#participants").val(i);
    } else {
      removeRow($(this));
      var i = Number(p)-1;
      $("#participants").val(i);
    }
    });
    $("#participants").change(function () {
    var i = 0;
    p = $("#participants").val();
    var rowCount = $("#participantTable div").length - 2;
    if(p > rowCount) {
      for(i=rowCount; i<p; i+=1){
        addRow();
      }
      $("#participantTable #addButtonRow").appendTo("#participantTable");
    } else if(p < rowCount) {
    }
    });
    </script>
