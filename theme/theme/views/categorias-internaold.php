<!-- Item 01 -->
<div class="item background-cover fondo-slider01">
    <div class="container-fluid titulo-slider-top texto-blanco text-shadow">
        <div class="col-md-12 padding0 text-center"><h1>Viajes</h1></div>
    </div>
    <div class="container-fluid lista-slider-top texto-blanco">
        <div class="col-md-4">
            <div class="listado-slider-interna"><a href="">Hoteles de Playa<br>(127)</a></div>
        </div>
        <div class="col-md-4">
            <div class="listado-slider-interna"><a href="">Hoteles románticos<br>(165)</a></div>
        </div>
        <div class="col-md-4">
            <div class="listado-slider-interna"><a href="">Hoteles de lujo<br>(127)</a></div>
        </div>
    </div>
</div>

<!-- Snacktrend -->
<div class="container-fluid fondo-gradient-rosa">
    <?php include('snacktrend.php');?>
</div>

<div class="container-fluid contenedor-ads fondo-gradient-gris-oscuro">
    <div class="container">

        <div class="row">
            <h2 class="titulo-gastronomia-categorias texto-blanco">Entretenimiento y Humor</h2>
        </div>

        <div class="row">
          <!-- Tarjeta -->
          <div class="col-sm-3">
              <div class="contenedor-tarjeta-blanco fondo-blanco">

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                      <div class="col-xs-6 col-sm-6" title="Vistas de la Lista" style="padding-right:0px; padding-left:0px;">
                          <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 24</div>
                      </div>

                      <div class="col-xs-6 col-sm-6" title="Fecha de creación de la Lista" style="padding-right:0px; padding-left:0px;">
                          <div class="fecha-tarjetas icon-vistas">08/09/2018</div>
                      </div>
                  </div>

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                    	<a href="http://www.labtico.com/subcategorias.html">
                    			<span class="categoria-snacktrend">Categoría/Perros</span>
                    	</a>
                      <a href="http://www.labtico.com/lista/5-1wasd">
                          <div class="header-tarjeta-categoria texto-gris-oscuro"><h3>example2</h3></div>
                          <div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQw-NggolPZy9ceD757BALF2yn3SxaJWuaQ-kch48RqguFGd6IwSenE8I1G)">
                                    <div class="top-listado-tarjeta texto-blanco">1</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento1</b><br>asd</div>
                              </div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(http://www.labtico.com/theme/theme/assets/img/Perfiles/perfil.jpg)">
                                    <div class="top-listado-tarjeta texto-blanco">2</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento2</b><br>asd</div>
                              </div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(http://www.labtico.com/theme/theme/assets/img/Perfiles/perfil.jpg)">
                                    <div class="top-listado-tarjeta texto-blanco">3</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento3</b><br>asd</div>
                              </div>
                          </div>
                      </a>
      						</div>

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias">
                          	<img src="http://www.labtico.com/assets/grocery_crud/css/jquery_plugins/cropper/vacio.png" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
                          </div>
                          <div class="username-tarjetas"><b> joncar15 </b><br>Votos:57</div>
                      </div>
      						</div>

              </div>
          </div>
          <!-- Tarjeta -->

          <!-- Tarjeta -->
          <div class="col-sm-3">
              <div class="contenedor-tarjeta-blanco fondo-blanco">

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                      <div class="col-xs-6 col-sm-6" title="Vistas de la Lista" style="padding-right:0px; padding-left:0px;">
                          <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 24</div>
                      </div>

                      <div class="col-xs-6 col-sm-6" title="Fecha de creación de la Lista" style="padding-right:0px; padding-left:0px;">
                          <div class="fecha-tarjetas icon-vistas">08/09/2018</div>
                      </div>
                  </div>

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                    	<a href="http://www.labtico.com/subcategorias.html">
                    			<span class="categoria-snacktrend">Categoría/Perros</span>
                    	</a>
                      <a href="http://www.labtico.com/lista/5-1wasd">
                          <div class="header-tarjeta-categoria texto-gris-oscuro"><h3>example2</h3></div>
                          <div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQw-NggolPZy9ceD757BALF2yn3SxaJWuaQ-kch48RqguFGd6IwSenE8I1G)">
                                    <div class="top-listado-tarjeta texto-blanco">1</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento1</b><br>asd</div>
                              </div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(http://www.labtico.com/theme/theme/assets/img/Perfiles/perfil.jpg)">
                                    <div class="top-listado-tarjeta texto-blanco">2</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento2</b><br>asd</div>
                              </div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(http://www.labtico.com/theme/theme/assets/img/Perfiles/perfil.jpg)">
                                    <div class="top-listado-tarjeta texto-blanco">3</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento3</b><br>asd</div>
                              </div>
                          </div>
                      </a>
      						</div>

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias">
                          	<img src="http://www.labtico.com/assets/grocery_crud/css/jquery_plugins/cropper/vacio.png" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
                          </div>
                          <div class="username-tarjetas"><b> joncar15 </b><br>Votos:57</div>
                      </div>
      						</div>

              </div>
          </div>
          <!-- Tarjeta -->

          <!-- Tarjeta -->
          <div class="col-sm-3">
              <div class="contenedor-tarjeta-blanco fondo-blanco">

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                      <div class="col-xs-6 col-sm-6" title="Vistas de la Lista" style="padding-right:0px; padding-left:0px;">
                          <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 24</div>
                      </div>

                      <div class="col-xs-6 col-sm-6" title="Fecha de creación de la Lista" style="padding-right:0px; padding-left:0px;">
                          <div class="fecha-tarjetas icon-vistas">08/09/2018</div>
                      </div>
                  </div>

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                    	<a href="http://www.labtico.com/subcategorias.html">
                    			<span class="categoria-snacktrend">Categoría/Perros</span>
                    	</a>
                      <a href="http://www.labtico.com/lista/5-1wasd">
                          <div class="header-tarjeta-categoria texto-gris-oscuro"><h3>example2</h3></div>
                          <div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQw-NggolPZy9ceD757BALF2yn3SxaJWuaQ-kch48RqguFGd6IwSenE8I1G)">
                                    <div class="top-listado-tarjeta texto-blanco">1</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento1</b><br>asd</div>
                              </div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(http://www.labtico.com/theme/theme/assets/img/Perfiles/perfil.jpg)">
                                    <div class="top-listado-tarjeta texto-blanco">2</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento2</b><br>asd</div>
                              </div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(http://www.labtico.com/theme/theme/assets/img/Perfiles/perfil.jpg)">
                                    <div class="top-listado-tarjeta texto-blanco">3</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento3</b><br>asd</div>
                              </div>
                          </div>
                      </a>
      						</div>

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias">
                          	<img src="http://www.labtico.com/assets/grocery_crud/css/jquery_plugins/cropper/vacio.png" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
                          </div>
                          <div class="username-tarjetas"><b> joncar15 </b><br>Votos:57</div>
                      </div>
      						</div>

              </div>
          </div>
          <!-- Tarjeta -->

          <!-- Tarjeta -->
          <div class="col-sm-3">
              <div class="contenedor-tarjeta-blanco fondo-blanco">

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                      <div class="col-xs-6 col-sm-6" title="Vistas de la Lista" style="padding-right:0px; padding-left:0px;">
                          <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 24</div>
                      </div>

                      <div class="col-xs-6 col-sm-6" title="Fecha de creación de la Lista" style="padding-right:0px; padding-left:0px;">
                          <div class="fecha-tarjetas icon-vistas">08/09/2018</div>
                      </div>
                  </div>

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                    	<a href="http://www.labtico.com/subcategorias.html">
                    			<span class="categoria-snacktrend">Categoría/Perros</span>
                    	</a>
                      <a href="http://www.labtico.com/lista/5-1wasd">
                          <div class="header-tarjeta-categoria texto-gris-oscuro"><h3>example2</h3></div>
                          <div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQw-NggolPZy9ceD757BALF2yn3SxaJWuaQ-kch48RqguFGd6IwSenE8I1G)">
                                    <div class="top-listado-tarjeta texto-blanco">1</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento1</b><br>asd</div>
                              </div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(http://www.labtico.com/theme/theme/assets/img/Perfiles/perfil.jpg)">
                                    <div class="top-listado-tarjeta texto-blanco">2</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento2</b><br>asd</div>
                              </div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(http://www.labtico.com/theme/theme/assets/img/Perfiles/perfil.jpg)">
                                    <div class="top-listado-tarjeta texto-blanco">3</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento3</b><br>asd</div>
                              </div>
                          </div>
                      </a>
      						</div>

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias">
                          	<img src="http://www.labtico.com/assets/grocery_crud/css/jquery_plugins/cropper/vacio.png" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
                          </div>
                          <div class="username-tarjetas"><b> joncar15 </b><br>Votos:57</div>
                      </div>
      						</div>

              </div>
          </div>
          <!-- Tarjeta -->

        </div>

    </div>
</div>


<div class="container-fluid contenedor-ads fondo-gradient-gris-claro">
    <div class="container">

        <div class="row">
            <h2 class="titulo-gastronomia-categorias texto-blanco">Entretenimiento y Humor</h2>
        </div>

        <div class="row">

          <!-- Tarjeta -->
          <div class="col-sm-3">
              <div class="contenedor-tarjeta-blanco fondo-blanco">

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                      <div class="col-xs-6 col-sm-6" title="Vistas de la Lista" style="padding-right:0px; padding-left:0px;">
                          <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 24</div>
                      </div>

                      <div class="col-xs-6 col-sm-6" title="Fecha de creación de la Lista" style="padding-right:0px; padding-left:0px;">
                          <div class="fecha-tarjetas icon-vistas">08/09/2018</div>
                      </div>
                  </div>

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                    	<a href="http://www.labtico.com/subcategorias.html">
                    			<span class="categoria-snacktrend">Categoría/Perros</span>
                    	</a>
                      <a href="http://www.labtico.com/lista/5-1wasd">
                          <div class="header-tarjeta-categoria texto-gris-oscuro"><h3>example2</h3></div>
                          <div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQw-NggolPZy9ceD757BALF2yn3SxaJWuaQ-kch48RqguFGd6IwSenE8I1G)">
                                    <div class="top-listado-tarjeta texto-blanco">1</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento1</b><br>asd</div>
                              </div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(http://www.labtico.com/theme/theme/assets/img/Perfiles/perfil.jpg)">
                                    <div class="top-listado-tarjeta texto-blanco">2</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento2</b><br>asd</div>
                              </div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(http://www.labtico.com/theme/theme/assets/img/Perfiles/perfil.jpg)">
                                    <div class="top-listado-tarjeta texto-blanco">3</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento3</b><br>asd</div>
                              </div>
                          </div>
                      </a>
      						</div>

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias">
                          	<img src="http://www.labtico.com/assets/grocery_crud/css/jquery_plugins/cropper/vacio.png" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
                          </div>
                          <div class="username-tarjetas"><b> joncar15 </b><br>Votos:57</div>
                      </div>
      						</div>

              </div>
          </div>
          <!-- Tarjeta -->

          <!-- Tarjeta -->
          <div class="col-sm-3">
              <div class="contenedor-tarjeta-blanco fondo-blanco">

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                      <div class="col-xs-6 col-sm-6" title="Vistas de la Lista" style="padding-right:0px; padding-left:0px;">
                          <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 24</div>
                      </div>

                      <div class="col-xs-6 col-sm-6" title="Fecha de creación de la Lista" style="padding-right:0px; padding-left:0px;">
                          <div class="fecha-tarjetas icon-vistas">08/09/2018</div>
                      </div>
                  </div>

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                    	<a href="http://www.labtico.com/subcategorias.html">
                    			<span class="categoria-snacktrend">Categoría/Perros</span>
                    	</a>
                      <a href="http://www.labtico.com/lista/5-1wasd">
                          <div class="header-tarjeta-categoria texto-gris-oscuro"><h3>example2</h3></div>
                          <div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQw-NggolPZy9ceD757BALF2yn3SxaJWuaQ-kch48RqguFGd6IwSenE8I1G)">
                                    <div class="top-listado-tarjeta texto-blanco">1</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento1</b><br>asd</div>
                              </div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(http://www.labtico.com/theme/theme/assets/img/Perfiles/perfil.jpg)">
                                    <div class="top-listado-tarjeta texto-blanco">2</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento2</b><br>asd</div>
                              </div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(http://www.labtico.com/theme/theme/assets/img/Perfiles/perfil.jpg)">
                                    <div class="top-listado-tarjeta texto-blanco">3</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento3</b><br>asd</div>
                              </div>
                          </div>
                      </a>
      						</div>

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias">
                          	<img src="http://www.labtico.com/assets/grocery_crud/css/jquery_plugins/cropper/vacio.png" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
                          </div>
                          <div class="username-tarjetas"><b> joncar15 </b><br>Votos:57</div>
                      </div>
      						</div>

              </div>
          </div>
          <!-- Tarjeta -->

          <!-- Tarjeta -->
          <div class="col-sm-3">
              <div class="contenedor-tarjeta-blanco fondo-blanco">

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                      <div class="col-xs-6 col-sm-6" title="Vistas de la Lista" style="padding-right:0px; padding-left:0px;">
                          <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 24</div>
                      </div>

                      <div class="col-xs-6 col-sm-6" title="Fecha de creación de la Lista" style="padding-right:0px; padding-left:0px;">
                          <div class="fecha-tarjetas icon-vistas">08/09/2018</div>
                      </div>
                  </div>

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                    	<a href="http://www.labtico.com/subcategorias.html">
                    			<span class="categoria-snacktrend">Categoría/Perros</span>
                    	</a>
                      <a href="http://www.labtico.com/lista/5-1wasd">
                          <div class="header-tarjeta-categoria texto-gris-oscuro"><h3>example2</h3></div>
                          <div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQw-NggolPZy9ceD757BALF2yn3SxaJWuaQ-kch48RqguFGd6IwSenE8I1G)">
                                    <div class="top-listado-tarjeta texto-blanco">1</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento1</b><br>asd</div>
                              </div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(http://www.labtico.com/theme/theme/assets/img/Perfiles/perfil.jpg)">
                                    <div class="top-listado-tarjeta texto-blanco">2</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento2</b><br>asd</div>
                              </div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(http://www.labtico.com/theme/theme/assets/img/Perfiles/perfil.jpg)">
                                    <div class="top-listado-tarjeta texto-blanco">3</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento3</b><br>asd</div>
                              </div>
                          </div>
                      </a>
      						</div>

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias">
                          	<img src="http://www.labtico.com/assets/grocery_crud/css/jquery_plugins/cropper/vacio.png" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
                          </div>
                          <div class="username-tarjetas"><b> joncar15 </b><br>Votos:57</div>
                      </div>
      						</div>

              </div>
          </div>
          <!-- Tarjeta -->

          <!-- Tarjeta -->
          <div class="col-sm-3">
              <div class="contenedor-tarjeta-blanco fondo-blanco">

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                      <div class="col-xs-6 col-sm-6" title="Vistas de la Lista" style="padding-right:0px; padding-left:0px;">
                          <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 24</div>
                      </div>

                      <div class="col-xs-6 col-sm-6" title="Fecha de creación de la Lista" style="padding-right:0px; padding-left:0px;">
                          <div class="fecha-tarjetas icon-vistas">08/09/2018</div>
                      </div>
                  </div>

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                    	<a href="http://www.labtico.com/subcategorias.html">
                    			<span class="categoria-snacktrend">Categoría/Perros</span>
                    	</a>
                      <a href="http://www.labtico.com/lista/5-1wasd">
                          <div class="header-tarjeta-categoria texto-gris-oscuro"><h3>example2</h3></div>
                          <div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQw-NggolPZy9ceD757BALF2yn3SxaJWuaQ-kch48RqguFGd6IwSenE8I1G)">
                                    <div class="top-listado-tarjeta texto-blanco">1</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento1</b><br>asd</div>
                              </div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(http://www.labtico.com/theme/theme/assets/img/Perfiles/perfil.jpg)">
                                    <div class="top-listado-tarjeta texto-blanco">2</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento2</b><br>asd</div>
                              </div>
                              <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(http://www.labtico.com/theme/theme/assets/img/Perfiles/perfil.jpg)">
                                    <div class="top-listado-tarjeta texto-blanco">3</div>
                                  </div>
                                  <div class="listado-tarjeta"><b>elemento3</b><br>asd</div>
                              </div>
                          </div>
                      </a>
      						</div>

      						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias">
                          	<img src="http://www.labtico.com/assets/grocery_crud/css/jquery_plugins/cropper/vacio.png" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
                          </div>
                          <div class="username-tarjetas"><b> joncar15 </b><br>Votos:57</div>
                      </div>
      						</div>

              </div>
          </div>
          <!-- Tarjeta -->

        </div>

    </div>
</div>


<!--
    <div class="container-fluid contenedor-ads">
        <div class="container">
            <div class="row margin-categorias-tarjetas">
                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                      <td>Animales y Mascotas</td>
                                  </tr>
                                  <tr>
                                      <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                      <td>Animales y Mascotas</td>
                                  </tr>
                                  <tr>
                                      <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                      <td>Animales y Mascotas</td>
                                  </tr>
                                  <tr>
                                      <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                      <td>Animales y Mascotas</td>
                                  </tr>
                                  <tr>
                                      <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                      <td>Animales y Mascotas</td>
                                  </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>
            </div>

            <div class="row margin-categorias-tarjetas">
                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>
            </div>


            <div class="row margin-categorias-tarjetas">
                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block">
                                          <img src="http://www.labtico.com/theme/theme/assets/img/Iconos/icono-gastronomia.png" alt="Perfil Snacktrend" class="img-responsive img-circle">
                                      </div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                                <tr>
                                    <td><a href="<?= base_url() ?>subcategorias.html">127</a></td>
                                    <td>Animales y Mascotas</td>
                                </tr>
                              </tbody>
                          </table>
                     </div>
                </div>
            </div>

        </div>
    </div>-->

    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php include('footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php include('footer-mapa.php');?>
    </footer>

    <!-- Modales -->
    <?php include('modales.php');?>

    <!-- Librerias -->
    <?php include('librerias.php');?>
