<!-- Item 01 -->
<div class="item background-cover fondo-slider01 margin-secciones-menu-top" <?= !empty($categoria->banner)?'style="background:url(\''.base_url('img/categorias/'.$categoria->banner).'\'); background-repeat:no-repeat; background-size:cover;"':''; ?>>
    <div class="container-fluid titulo-slider-top texto-blanco text-shadow">
        <div class="col-md-12 padding0 text-center"><h1><?= $categoria->nombre ?></h1></div>
    </div>
    <div class="container-fluid lista-slider-top texto-blanco">
        <?php
          $this->db->select('sub_categorias.id, sub_categorias.nombre, COUNT(listas_categorias.id) AS total');
          $this->db->join('sub_categorias','sub_categorias.id = listas_categorias.subcategorias_id');
          $this->db->group_by('listas_categorias.subcategorias_id');
          $this->db->limit(3);
          $this->db->order_by('COUNT(listas_categorias.id) DESC');
          foreach($this->db->get_where('listas_categorias',array('sub_categorias.categorias_id'=>$categoria->id))->result() as $s):
            $this->db->group_by('listas_id'); 
            $total = $this->db->get_where('listas_categorias',array('subcategorias_id'=>$s->id))->num_rows();
        ?>
          <div class="col-md-4">
              <div class="listado-slider-interna">
                <a href="<?= base_url('subcategoria/'.toUrl($s->id.'-'.$s->nombre)) ?>"><?= $s->nombre ?><br>(<?= $total ?>)</a>
              </div>
          </div>
        <?php endforeach ?>

    </div>
</div>

<!-- Snacktrend -->
<div class="container-fluid fondo-gradient-rosa">
    <?php $this->db->where('categorias.id',$categoria->id); ?>
    <?php include('snacktrend.php');?>
</div>

<?php $this->db->order_by('sub_categorias.nombre','ASC'); foreach($this->db->get_where('sub_categorias',array('categorias_id'=>$categoria->id))->result() as $n=>$c): ?>

    <div class="container-fluid contenedor-ads fondo-gradient-gris-<?= $n%2==0?'oscuro':'claro' ?>">
        <div class="container">

            <div class="row">
                <h2 class="titulo-gastronomia-categorias texto-blanco">
                  <a href="<?= base_url('subcategoria/'.toUrl($c->id.'-'.$c->nombre)) ?>">
                    <?= $c->nombre ?> (<?php  $this->db->group_by('listas_id'); echo $this->db->get_where('listas_categorias',array('categorias_id'=>$categoria->id,'subcategorias_id'=>$c->id))->num_rows() ?>)
                  </a> 
                </h2>
            </div>

            <div class="row">

              <?php
                $listas = $this->querys->get_listas(array('sub_categorias.id'=>$c->id));
                foreach($listas->result() as $l):
              ?>

              <!-- Tarjeta -->
              <div class="col-sm-3">
                  <div class="contenedor-tarjeta-blanco-categorias fondo-blanco">

          						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                          <div class="col-xs-6 col-sm-6" title="Vistas de la Lista" style="padding-right:0px; padding-left:0px;">
                              <div class="vistas-tarjetas icon-vistas">
                                <i class="fa fa-eye" aria-hidden="true"></i> <?= $l->vistas ?>
                              </div>
                          </div>

                          <div class="col-xs-6 col-sm-6" title="Fecha de creación de la Lista" style="padding-right:0px; padding-left:0px;">
                              <div class="fecha-tarjetas icon-vistas"><?= $l->fecha_format ?></div>
                          </div>
                      </div>

                      <div class="lista-3posiciones">
              						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">                            	
                              <a href="<?= $l->link ?>">
                                  <div class="header-tarjeta-categoria texto-gris-oscuro"><h3><?= $l->titulo_corto ?></h3></div>
                                  <div>
                                    <?php foreach($l->detalles_sort as $d): ?>
                                      <div class="col-xs-12 col-sm-12 contenedor-listado-categoria">
                                          <div class="imagen-listado-tarjeta" style="background:url('<?= $d->adjunto ?>')">
                                            <div class="top-listado-tarjeta texto-blanco"><?= $d->posicion ?></div>
                                          </div>
                                          <div class="listado-tarjeta"><b><?= $d->nombre ?></b><br><?= $d->autor ?></div>
                                      </div>
                                    <?php endforeach ?>
                                  </div>
                              </a>
              						</div>
                      </div>

          						<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
                          <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                              <div class="perfil-tarjeta-categorias-slider" style="background:url(<?= $l->perfil; ?>); background-size: cover; background-position: center; border-radius:100%;">
                              	<!--<img src="<?= $l->perfil; ?>" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">-->
                              </div>
                              <div class="username-tarjetas"><b> <?= $l->username ?> </b><br>Votos:<?= $l->votos ?></div>
                          </div>
          						</div>

                  </div>
              </div>
              <!-- Tarjeta -->
              <?php endforeach ?>


            </div>

        </div>
    </div>
<?php endforeach ?>



    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php include('footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php include('footer-mapa.php');?>
    </footer>

    <!-- Modales -->
    <?php include('modales.php');?>

    <!-- Librerias -->
    <?php include('librerias.php');?>
