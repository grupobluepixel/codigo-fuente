<div id="slider-gris-01" class="contenedor-recomendaciones">
    <div class="row titulo-redeem texto-negro"><h2><b>Recomendaciones</b></h2></div>

    <div class="row slider-gris-01">
      <div class="col-sm-12">
      <!-- Tarjeta -->
      <?php
        if(empty($recomendaciones)){
            $this->db->limit(4);
            $this->db->order_by('id','DESC');
            $recomendaciones = $this->querys->get_listas();
        }
      ?>
      <?php foreach($recomendaciones->result() as $row): ?>














        <!-- Tarjeta -->
        <div class="col-sm-3">
            <div class="contenedor-tarjeta-blanco fondo-blanco">

                  <div class="col-sm-12 texto-gris-oscuro text-left">
                      <h3><a href="<?= base_url('editar-lista/'.toUrl($row->id.'-'.$row->titulo_corto)) ?>"><span class="categoria-snacktrend"><?= $row->catnom ?></span></a></h3>
                  </div>

                  <a href="<?= $row->link ?>">
                      <div class="col-sm-12">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3><?= $row->titulo_corto ?></h3>
                          </div>

                          <?php get_instance()->db->limit(3); ?>
                          <?php foreach(get_instance()->db->get_where('listas_detalles',array('listas_id'=>$row->id))->result() as $detall): ?>
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta" style="background:url(<?= get_instance()->querys->get_foto($detall->adjunto) ?>)">
                                    <div class="top-listado-tarjeta texto-blanco"><?= $detall->posicion ?></div>
                                  </div>
                                  <div class="listado-tarjeta"><b><?= $detall->nombre ?></b><br><?= $detall->autor ?></div>
                              </div>
                          <?php endforeach ?>

                      </div>
                  </a>

                  <div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
        							<div class="titulo-tarjeta-categoria texto-gris-oscuro">
        									<div class="perfil-tarjeta-categorias">
        										  <img src="<?= $row->perfil ?>" alt="Perfil Snacktrend" class="center-block img-responsive img-circle" style="width: 30px;height: 30px;">
        									</div>
        									<div class="username-tarjetas"><b><?= $row->username ?></b><br>Votos:<?= $row->votos ?></div>
        							</div>
        					</div>

            </div>
        </div>
      <?php endforeach ?>
    </div>


    </div>
</div>
