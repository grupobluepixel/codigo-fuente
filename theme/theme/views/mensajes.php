
    <div class="container-fluid fondo-blanco">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 contenedor-ads">Google ADS</div>
            </div>
        </div>
    </div>

    <?= $output ?>

    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php include('footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php include('footer-mapa.php');?>
    </footer>

    <!-- Modales -->
    <?php include('modales.php');?>

    <!-- Librerias -->
    <?php include('librerias.php');?>

    <!-- Add / Remove -->
    <script>
    /* Variables */
