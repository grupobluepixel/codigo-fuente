<?php
	$categoria = $this->db->get_where('categorias',array('id'=>$c->categorias_id));
	if($categoria->num_rows()):
	$categoria = $categoria->row();
?>
<div class="fondo-gradient-gris">
	<div class="container-fluid">
		<div class="row margin-titulo-slider-home">
			<h2 class="titulo-gastronomia texto-blanco">
			<a href="<?= base_url('categoria/'.toUrl($categoria->id.'-'.$categoria->nombre)) ?>">
				<img src="<?= base_url('img/categorias/'.$categoria->icono) ?>">
				<?= $categoria->nombre ?>
			</a>
			</h2>
		</div>
	</div>
	<div id="slider-gris-01">
		<div class="row owl-carousel owl-theme slider_categorias_home">
			<!-- Tarjeta -->
			<?php
				$this->db->select('listas.*, categorias.id as catid, sub_categorias.id as subcatid, categorias.nombre as catnom, sub_categorias.nombre as subcatnom, user.nickname as username, user.foto as perfil');
				$this->db->join('user','user.id = listas.user_id');
				$this->db->join('listas_categorias','listas_categorias.listas_id = listas.id');
				$this->db->join('categorias','categorias.id = listas_categorias.categorias_id');
				$this->db->join('sub_categorias','sub_categorias.id = listas_categorias.subcategorias_id');
				$this->db->group_by('listas.id');
				foreach($this->db->get_where('listas',array('listas_categorias.categorias_id'=>$categoria->id))->result() as $ca):
			?>
			<div class="item item-slider-tarjetas">
				<div class="contenedor-tarjeta-blanco fondo-blanco">
					<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
						<div class="col-xs-6 col-sm-6" title="Vistas de la Lista" style="padding-right:0px; padding-left:0px;">
							<div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> <?= $ca->vistas ?></div>
						</div>
						<div class="col-xs-6 col-sm-6" title="Fecha de creación de la Lista" style="padding-right:0px; padding-left:0px;">
							<div class="fecha-tarjetas icon-vistas"><?= date("d/m/Y",strtotime($ca->fecha)) ?></div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">

						<span class="categoria-snacktrend">
							<a href="<?= base_url('categoria/'.toUrl($ca->catid.'-'.$ca->catnom)) ?>"><?= $ca->catnom; ?></a>
							/
							<a href="<?= base_url('subcategoria/'.toUrl($ca->subcatid.'-'.$ca->subcatnom)) ?>"><?= $ca->subcatnom; ?></a>
						</span>

						<a href="<?= base_url('lista/'.toUrl($ca->id.'-'.$ca->titulo_corto)) ?>" class="link-tarjetas">
							<div class="header-tarjeta-categoria"><h3><?= $ca->titulo ?></h3></div>

							<div class="height-listas-principales">
								<?php foreach($this->querys->get_sort($ca->id) as $detall):
									$detall->foto = $this->querys->get_foto($detall->adjunto);
								?>
								<div class="col-xs-12 col-sm-12" style="display:flex; margin-bottom:10px;">
										<div class="imagen-listado-tarjeta" style="background:url('<?= $this->querys->get_foto($detall->adjunto) ?>'); background-size: cover; background-position:center center; width: 50px;height: 50px;">
											<div class="top-listado-tarjeta texto-blanco" style="width: 50px;height: 30px;"><?= $detall->posicion ?></div>
										</div>
										<div class="listado-tarjeta"><b><?= $detall->nombre ?></b><br><?= $detall->autor ?></div>
								</div>
								<?php endforeach ?>
							</div>

						</a>

					</div>

					<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
							<div class="titulo-tarjeta-categoria texto-gris-oscuro">
									<div class="perfil-tarjeta-categorias">
										<img src="<?= $this->querys->get_perfil($ca->perfil) ?>" alt="Perfil Snacktrend" class="center-block img-responsive img-circle" style="width:30px; height:30px;">
									</div>
									<div class="username-tarjetas"><b> <?= $ca->username ?> </b><br>Votos:<?= $ca->votos ?></div>
							</div>
					</div>

				</div>
			</div>
			<?php endforeach ?>
		</div>
	</div>
	<?php endif ?>
</div>
<script>
	$('.slider_categorias_home').owlCarousel({
stagePadding: 50,
loop:false,
margin:10,
nav:false,
autoplay:true,
autoplayTimeout:3000,
autoplayHoverPause:true,
responsive:{
0:{
items:1
},
600:{
items:3
},
1000:{
items:4
}
}
});
</script>
