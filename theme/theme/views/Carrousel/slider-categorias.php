<div id="slider-categorias">
    <div class="owl-carousel owl-theme slider-categorias">
          <?php 
              $this->db->limit(5);
              $this->db->join('categorias','categorias.id = categorias_destacadas.categorias_id');
              foreach($this->db->get_where('categorias_destacadas')->result() as $c): ?>
              <a href="<?= base_url('categoria/'.toUrl($c->id.'-'.$c->nombre)) ?>"><div class="item item-slider-categorias"><h4><?= $c->nombre ?></h4></div></a>
          <?php endforeach ?>          

    </div>

    <script>
        var owl = $('.slider-categorias');
        owl.owlCarousel({
          items:7,
          loop:false,
          margin:10,
          autoplay:true,
          autoplayTimeout:2000,
          autoplayHoverPause:true,
          responsive:{
              0:{
                  items:2
              },
              600:{
                  items:3
              },
              1000:{
                  items:6
              }
          }
        });
        $('.play').on('click',function(){
          owl.trigger('play.owl.autoplay',[2000])
        })
        $('.stop').on('click',function(){
          owl.trigger('stop.owl.autoplay')
        })
    </script>
</div>
