

    <div class="container-fluid container-fluid fondo-gradient-gris-oscuro contenedor-listado-tarjetas">
        <div class="container">

          <div class="row">
              <h2 class="titulo-gastronomia-categorias texto-blanco">
                  <img src="http://labtico.com/img/categorias/7ec7b-icono-entretenimiento.png">Entretenimiento y Humor
              </h2>
          </div>

          <div class="row margin-subcategorias">
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <!--<h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>-->
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="<?= base_url() ?>theme/theme/assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <!--<h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>-->
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="<?= base_url() ?>theme/theme/assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <!--<h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>-->
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="<?= base_url() ?>theme/theme/assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <!--<h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>-->
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="<?= base_url() ?>theme/theme/assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
          </div>

          <div class="row">
              <h2 class="titulo-gastronomia-categorias texto-blanco">
                  <img src="http://labtico.com/img/categorias/7ec7b-icono-entretenimiento.png">Entretenimiento y Humor
              </h2>
          </div>

          <div class="row margin-subcategorias">
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <!--<h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>-->
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="<?= base_url() ?>theme/theme/assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <!--<h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>-->
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="<?= base_url() ?>theme/theme/assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <!--<h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>-->
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="<?= base_url() ?>theme/theme/assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <!--<h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>-->
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="<?= base_url() ?>theme/theme/assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
          </div>

          <div class="row">
              <h2 class="titulo-gastronomia-categorias texto-blanco">
                  <img src="http://labtico.com/img/categorias/7ec7b-icono-entretenimiento.png">Entretenimiento y Humor
              </h2>
          </div>

          <div class="row">
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <!--<h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>-->
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="<?= base_url() ?>theme/theme/assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <!--<h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>-->
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="<?= base_url() ?>theme/theme/assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <!--<h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>-->
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="<?= base_url() ?>theme/theme/assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <!--<h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>-->
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="<?= base_url() ?>theme/theme/assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
          </div>

        </div>
    </div>

    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php include('footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php include('footer-mapa.php');?>
    </footer>

    <!-- Modales -->
    <?php include('modales.php');?>

    <!-- Librerias -->
    <?php include('librerias.php');?>
